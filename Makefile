#TOOLCHAI_DIR =


TCPREFIX = arm-none-eabi-
CC	= $(TCPREFIX)gcc
LD	= $(TCPREFIX)gcc
CP	= $(TCPREFIX)objcopy
OD	= $(TCPREFIX)objdump
#GDBTUI	= $(TCPREFIX)gdbtui

TARGET = main.bin

CFLAGS	+= -I . \
           -I sources/tests \
           -I sources/platform \
           -I sources/display \
           -I sources/mnemonics \
           -I sources/qrcode \
           -I sources/monero \
           -I sources/messenger \
           -I sources/crypto \
           -I sources/atecc508 \
           -I sources \
           -I sources/atecc508 \
           -I sources/nanopb \
           -I submodules/nanopb \
           -c -fno-common \
           -O0 \
           -g \
           -mcpu=cortex-m4 \
           -std=gnu11 \
           -W \
           -Wall \
           -Wextra \
           -Wunused-but-set-variable

#-mfpu=fpv4-sp-d16 -mfloat-abi=hard

#            -Wimplicit-function-declaration \
#            -Wredundant-decls \
#            -Wstrict-prototypes \
#            -Wundef \
#            -Wshadow \
#            -Wpointer-arith \
#            -Wformat \
#            -Wreturn-type \
#            -Wsign-compare \
#            -Wmultichar \
#            -Wformat-nonliteral \
#            -Winit-self \
#            -Wuninitialized \
#            -Wformat-security \
#            -Werror \
#            -fno-common \
#            -fno-exceptions \
#            -fvisibility=internal \
#            -ffunction-sections \
#            -fdata-sections 
#            -fstack-protector-all 
LFLAGS	= -Tlinkfile/linker.ld -mthumb -nostartfiles -specs=nosys.specs -specs=nano.specs  -lgcc  -march=armv7e-m
CPFLAGS	= -Obinary
ODFLAGS	= -S -x

.PHONY: default all clean

default: $(TARGET)
all: default

##header files
#najde vsechny .c a pripravi z nich .o aby je pak mohl volat
OBJECTS =  	$(patsubst %.c, %.o, $(wildcard *.c)) \
		$(patsubst %.c, %.o, $(wildcard sources/platform/startup/*.c)) \
		$(patsubst %.S, %.o, $(wildcard sources/platform/startup/*.S)) \
		$(patsubst %.c, %.o, $(wildcard sources/*.c)) \
		$(patsubst %.c, %.o, $(wildcard sources/display/*.c)) \
		$(patsubst %.c, %.o, $(wildcard sources/crypto/*.c)) \
		$(patsubst %.c, %.o, $(wildcard sources/mnemonics/*.c)) \
		$(patsubst %.c, %.o, $(wildcard sources/qrcode/*.c)) \
                $(patsubst %.c, %.o, $(wildcard sources/atecc508/*.c)) \
		$(patsubst %.c, %.o, $(wildcard sources/nanopb/*.c)) \
		$(patsubst %.c, %.o, $(wildcard sources/tests/*.c)) \
		$(patsubst %.c, %.o, $(wildcard sources/monero/*.c)) \
                $(patsubst %.c, %.o, $(wildcard submodules/nanopb/*.c)) 
	  	
HEADERS = 	$(wildcard *.h) \
		$(wildcard sources/*.h) \
		$(wildcard sources/platform/*.h) \
		$(wildcard sources/display/*.h) \
		$(wildcard sources/crypto/*.h) \
		$(wildcard sources/mnemonics/*.h) \
		$(wildcard sources/qrcode/*.h)   \
		$(wildcard sources/monero/*.h)   \
		$(wildcard sources/tests/*.h)   \
		$(wildcard sources/nanopb/*.h)  \
		$(wildcard sources/atecc508/*.h)  \
		$(wildcard submodules/nanopb/*.h)

# $(wildcard *.c) da seznam vsech .c souboru v adreari
# $(patsubst %.c,%.o,$(wildcard *.c))   tohle vrati list .o souboru tak, ze veme list .c a vymeni jim koncovky

#all: run

clean:
	-rm -f main.lst sources/*.o \
	sources/*/*.o \
	sources/*/*/*.o \
	submodules/nanopb/*.o \
	*.o main.elf main.lst main.bin

%.o: %.c $(HEADERS)
#	@echo ".compiling others"
	$(CC) $(CFLAGS) -c $< -o $@ 
#all: library.cpp main.cpp,  $@ je all, $< library.cpp, $^ je library.cpp main.cpp

#run: main.bin

#.PRECIOUS: $(TARGET) $(OBJECTS)

main.elf: $(OBJECTS)
	$(LD) $(OBJECTS) $(LFLAGS) -o $@

$(TARGET): main.elf
	@echo "...copying"
	$(CP) $(CPFLAGS) main.elf main.bin
	$(OD) $(ODFLAGS) main.elf> main.lst

#main.elf: main.o linker_script.ld
#	@echo "..linking"
#	$(LD) $(LFLAGS) -o main.elf main.o

#main.o: main.c 
#	@echo ".compiling"
#	$(CC) $(CFLAGS) main.c

#debug:
#	$(GDBTUI) -ex "target remote localhost:3333" \
#	-ex "set remote hardware-breakpoint-limit 6" \
# 	-ex "set remote hardware-watchpoint-limit 4" main.elf

go: main.bin 
	python3 scripts/flash-image-maker.py
	python3 scripts/spi-flashing.py

erase: main.bin 
	python3 scripts/flash-erase.py


rst: main.bin 
	python3 scripts/device_reset.py

prepare: main.bin 
	python3 scripts/flash-image-maker.py
	flashrom -p ft2232_spi:type=2232H,port=A -c "SST26VF016B(A)" -w binary.bin #-V

