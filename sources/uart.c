/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "uart.h"
#include "CEC1702_defines.h"
#include "communication.h"
#include "util.h"
#include "menus.h"


void uart_init(void) {
  // UART GPIO's inicialization
  GPIO104_PIN_CONTROL_REGISTER = 0x1241UL;
  GPIO105_PIN_CONTROL_REGISTER = 0x1241UL;
  // UART0 initialization 115200 baud, 1stopbit, no parity:
  UART_CONFIGURATION_SELECT_REGISTER  = (uint8_t) 0x0;  // This must happen, uart block magic
  UART_LINE_CONTROL_REGISTER          = (uint8_t) 0x83; // DLAB=1, parity, word..
  UART_BAUDRATE_REGISTER_LSB          = (uint8_t) 0x1;  // 0x1 115200 baudrate,  0xC 9600baudrate
  UART_LINE_CONTROL_REGISTER          = (uint8_t) 0x3;  // DLAB=0, parity, word..
  UART_ACTIVATE_REGISTER              = (uint8_t) 0x1;  // ACTIVATE UART BLOCK! 
}


void uartSend(const char *string) {
  uint32_t i=0;
  while (string[i] != '\0') {
    uartSendChar(string[i]);
    i++;
  }
}


void uartSendChar(char send) {
  while (!(UART_LINE_STATUS_REGISTER & 0x20)) { //means that TRANSMIT_EMPTY == 1
    asm("mov r0,r0"); 
  } 
  UART_TRANSMIT_BUFFER_REGISTER = (uint8_t) send;
}


void uartSendByte(uint8_t send) {
  while (!(UART_LINE_STATUS_REGISTER & 0x20)) { //means that TRANSMIT_EMPTY == 1
    asm("mov r0,r0"); 
  } 
  UART_TRANSMIT_BUFFER_REGISTER = send;
}


void uartSendChar_to_hex(char send) {
  char hexa[3];
  // Hexify that char
  charToHex(send,hexa);
  // Send it over UART
  for (uint8_t i=0; i<3; i++){
    while (!(UART_LINE_STATUS_REGISTER & 0x20)) { // TRANSMIT_EMPTY == 1
      asm("mov r0,r0");
    } 
    UART_TRANSMIT_BUFFER_REGISTER = (uint8_t) hexa[i];
  }
}


uint8_t uartReceiveChar(uint32_t timeout) {
  uint32_t cycles = 0; 
  while(!(UART_LINE_STATUS_REGISTER & 0x01)) { //DATA READY
    asm("mov r0,r0");
    cycles++;
    if (cycles>timeout)
      return 0;
  } 
  return UART_RECEIVE_BUFFER_REGISTER;
}








