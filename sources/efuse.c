/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "efuse.h"
#include "CEC1702_defines.h"

#define REF_SEL1   (*((volatile uint32_t*) ((((((GPIO036_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))
#define REF_SEL2   (*((volatile uint32_t*) ((((((GPIO134_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))


bool writeSeed(uint8_t position, uint8_t *array) {
  efuseProgram( 2, position*0x20, array, 32);
  return true;//add check? fixme
}


uint16_t checkSetupBytes(void) {
  uint8_t bytes[4];

  efuseEnable();  
  //testing data:
  //uint8_t test[4] = {0xff,0x00,0x00,0x00};
  for(uint8_t i = 0; i<4; i++)
    //bytes[i] = test[i];
    bytes[i] = *(uint8_t*)(EFUSE_MEMORY_SETUP_BYTES + i);
  efuseDisable();
  uint8_t i = 0;
  while((bytes[i] == 0xFF) && i < 3) {//skip used and deleted seed position 
    i++;
  }
  uint16_t x = ((uint16_t)i<<8);//
  x |= bytes[i];
  return 0x01;
  //return (x);// DEBUG FIXME 
}


bool burnSetupByte(uint8_t byte_pos, uint8_t value) {
  if(byte_pos>3)
    return true;//error
  
  uint8_t byte_to_burn[1];
  byte_to_burn[0] = value;
  efuseProgram( 1, (EFUSE_MEMORY_SETUP_BYTES - EFUSE_BLOCK_1) + byte_pos, byte_to_burn, 1);

  if(*((uint8_t*)((EFUSE_MEMORY_SETUP_BYTES - EFUSE_BLOCK_1) + byte_pos)) == value)
    return 0;
  else
    return 1; 
}




/* 
Program OTP memory
bytes to burn
number of bytes to burn
*/
void efuseProgram( uint8_t block, uint8_t byte_address, uint8_t *bytes_to_burn, uint16_t number_of_bytes) {
  uint16_t bit_address = 8*byte_address;
  //set vref_adc to ground
  //vrefAdc152V();
  vrefAdcGnd();
  uartSend("vref to gnd\r\n");
  //wait 0.5s
  wait(500);
  uartSend("500ms wait\r\n");

  //enable efuse block
  EFUSE_CONTROL = 0x11;
  uartSend("ENABLE EFUSE BLOCK\r\n");

  //SET MAN_ENABLE BIT to 1 to enable manual programming
  EFUSE_MANUAL_CONTROL = 0x01;
  uartSend("enable manual control \r\n");

  //setting FSOURCE_EN_READ to 0, reading disabled
  EFUSE_CONTROL = 0x01;
  uartSend("FSOURCE EN READ TO 0\r\n");

  //setting FSOURCE_EN_PRGM to 1, programming enabled
  EFUSE_CONTROL = 0x19;
  uartSend("FSOURCE_EN_PRGM - connects eFuse FSOURCE pin to power\r\n");

  //setting FSOURCE_EXT_PGM to 0, programming will be done by registers
  EFUSE_CONTROL = 0x09;
  uartSend("clear fsource en read\r\n");

  //set external reference 1.52 to adc ref pin
  vrefAdc152V();
  wait(10);
  uartSend("vref to 1.52v\r\n");
  //IP_CS bit to 0b
  EFUSE_MANUAL_CONTROL = 0x03;

  uartSend("IP CS BIT TO 0 (enable efuse device)\r\n");

  uint16_t block_shifted = block << 10;
  EFUSE_MANUAL_MODE_ADDRESS = block_shifted; 
  
  uartSend("IP ADDR HI\r\n");
  //set the IP_CS bit to 1, this enables and powers the selected block
  EFUSE_MANUAL_CONTROL |= ((uint16_t)1 << 1);
  uartSend("IP CS TO 1 - BLOCK POWERED\r\n");
  // The certain 128B block is set, powered up and can be written into
  //
  // (if it isn't already locked of course) 
  uartSend("\r\n\r\nBurning data into efuse..\r\n");
  for (uint16_t x = 0; x < number_of_bytes; x++) { //pro kazdy byte udelam tohle:
    ledBlink(SHORT);
    uartSend("\r\nByte to burn [bits are flipped!]: ");
    uartSendChar_to_hex(bytes_to_burn[x]);
    uartSend("\r\n");
    
    for (uint16_t y = 0; y<8; y++) {
      if ((bytes_to_burn[x] >> y) & 0x1) {

        //uartSend("DEEPEST LOOP HERE\r\n");
        EFUSE_MANUAL_MODE_ADDRESS = block_shifted + (bit_address + 8*x + y);
        //wait 5us
        wait(5);

        //burn it!
        EFUSE_MANUAL_CONTROL |= (1UL << 2);
        wait(100);
        EFUSE_MANUAL_CONTROL &= ~(1UL << 2);
        wait(5);

        uartSend("1");
      }
      else {
        uartSend("0");
        }
    }
  }
  uartSend("efuse burned!\r\n");
  vrefAdcGnd();
  efuseDisable();
}



void efuseEnable(void) {
  EFUSE_CONTROL        = 0x01;         // reset and enable 
  EFUSE_CONTROL        = 0x11;         // reset and enable 
  EFUSE_MANUAL_CONTROL = 0x02;         // manual mode disabled
  wait_us(100);
}

void efuseDisable(void) {
  EFUSE_CONTROL        &= 0xFFFFFFFE;  //disable efuse
}



void checkEfuse(void) {
  uartSend("\r\n\r\n\r\n       DATA FROM EFUSE BLOCK, USER'S REGION (PERSISTENT STORAGE):\r\n");
  efuseEnable();
  uint8_t *p_to_block1_data = (uint8_t*) (EFUSE_MEMORY_PRIM_AUTH_PUBKEY);
  uint8_t block_data[384];
  memcpy(block_data, p_to_block1_data, 384);
  efuseDisable();

  for (uint16_t i = 0; i<384; i++) {
    if(i==0) {
      uartSend("\r\n####### Efuse block 1:");
      uartSend("\r\nAuthentication Public Key X: \r\n    ");
    }
    if(i==32)
      uartSend("\r\nAuthentication Public Key Y: \r\n    ");
    if(i==64)
      uartSend("\r\nDevice's HW key: \r\n    ");
    if(i==96)
      uartSend("\r\nSetup bytes:\r\n    ");
    if(i>96 && i<104)
      uartSend(" ");
    if(i==104)
      uartSend("\r\nDevice's visual identifier:\r\n    ");
    if(i==128) {
      uartSend("\r\n\r\n####### Efuse block 2:");
      uartSend("\r\nSeed 0: ");
    }
    if(i==160) {
      uartSend("\r\nSeed 1: ");
    }
    if(i==192) {
      uartSend("\r\nSeed 2: ");
    }
    if(i==224) {
      uartSend("\r\nSeed 3: ");
    }
    if(i==256) {
      uartSend("\r\nSeed 4: ");
    }
    if(i==288) {
      uartSend("\r\nAnother key?: ");
    }
    if(i==320) {
      uartSend("\r\nAnother key?: ");
    }
    if(i==352) {
      uartSend("\r\nWot da fok is diz: ");
    }
/* hardfault while reading over 384, why? FIXME
    if(i==384) {
      uartSend("\r\n\r\nEfuse block 3:");
      uartSend("\r\nseed 9:\r\n    ");
    }
*/
    uartSendChar_to_hex(block_data[i]);
  }
  uartSend("\r\n\r\n####### Others: ");
  p_to_block1_data = (uint8_t*) 0x40082010 + 480;
  memcpy(block_data, p_to_block1_data, 32);
  for (uint16_t i = 0; i<32; i++) {
    uartSendChar_to_hex(block_data[i]);   
  }
  
  uartSend("\r\nBit settings debug port:\r\n");
  for (uint8_t i = 0; i<8;i++) {
    if(i==0)
      uartSend("BIT[0] Undefined: ");
    if(i==1)
      uartSend("BIT[1] Undefined: ");
    if(i==2)
      uartSend("BIT[2] Undefined: ");
    if(i==3)
      uartSend("BIT[3] Undefined: ");
    if(i==4)
      uartSend("BIT[4] Undefined: ");
    if(i==5)
      uartSend("BIT[5] Undefined: ");
    if(i==6)
      uartSend("BIT[6] Debug select: ");
    if(i==7)
      uartSend("BIT[7] Debug Disable =0 (T/Eng)/ =1 (prod): ");
    if ((block_data[2] >> i) & 0x1) {
      if(i==0)
        uartSend("--> 1\r\n");
      if(i==1)
        uartSend("--> 1\r\n");
      if(i==2)
        uartSend("--> 1\r\n");
      if(i==3)
        uartSend("--> 1\r\n");
      if(i==4)
        uartSend("--> 1\r\n");
      if(i==5)
        uartSend("--> 1\r\n");
      if(i==6)
        uartSend("Debug port uses SWD\r\n");
      if(i==7)
        uartSend("Debug port disabled on ROM exit\r\n");
      }
      else {
      if(i==0)
        uartSend("--> 0\r\n");
      if(i==1)
        uartSend("--> 0\r\n");
      if(i==2)
        uartSend("--> 0\r\n");
      if(i==3)
        uartSend("--> 0\r\n");
      if(i==4)
        uartSend("--> 0\r\n");
      if(i==5)
        uartSend("--> 0\r\n");
      if(i==6)
        uartSend("Debug port uses JTAG\r\n");
      if(i==7)
        uartSend("Debug port enabled on ROM exit\r\n");
        }
  }
  uartSend("\r\nCustomer flags:\r\n");
  for (uint8_t i = 0; i<8;i++) {
    // bit description here
    if(i==0)
      uartSend("BIT[0] Authentication settings: ");
    if(i==1)
      uartSend("BIT[1] Private Key Encryption: ");
    if(i==2)
      uartSend("BIT[2] Private Key Encryption: ");
    if(i==3)
      uartSend("BIT[3] ECC508 Support: ");
    if(i==4)
      uartSend("BIT[4] Undefined: ");
    if(i==5)
      uartSend("BIT[5] Undefined: ");
    if(i==6)
      uartSend("BIT[6] Undefined: ");
    if(i==7)
      uartSend("BIT[7] eFuse AES Encryption Key Select: ");
    // bit values itself from here
    if ((block_data[3] >> i) & 0x1) {
      if(i==0)
        uartSend("--> Header and firmware authenticated with ECC public key stored in Qx and Qy\r\n");
      if(i==1)
        uartSend("--> Private ECC key (bytes 0-31) is AES-encrypted with ROM AES key\r\n");
      if(i==2)
        uartSend("--> Private ECC key (bytes 0-31) is locked\r\n");
      if(i==3)
        uartSend("--> ECC508 support enabled\r\n");
      if(i==4)
        uartSend("--> 1\r\n");
      if(i==5)
        uartSend("--> 1\r\n");
      if(i==6)
        uartSend("--> 1\r\n");
      if(i==7)
        uartSend("--> ROM Secure AES Encryption Key\r\n");
      }
      else {
      if(i==0)
        uartSend("--> Header and firmware checked with SHA-256\r\n");
      if(i==1)
        uartSend("--> Private ECC key does not need decryption\r\n");
      if(i==2)
        uartSend("--> Private ECC key (bytes 0-31) is not locked\r\n");
      if(i==3)
        uartSend("--> ECC508 support disabled\r\n");
      if(i==4)
        uartSend("--> 0\r\n");
      if(i==5)
        uartSend("--> 0\r\n");
      if(i==6)
        uartSend("--> 0\r\n");
      if(i==7)
        uartSend("--> Derived Secure AES Encryption Key\r\n");      
      }
  }
//FIXME: switch off reading bit
}

/* Pin settings for external switch for switching reference programming voltage */
void  vrefAdcInit(void)
{
  //Pin settings
  //ref sel 1 gpio 036   IN1
  //ref sel 2 gpio 134   IN2
  GPIO036_PIN_CONTROL_REGISTER = 0x240UL;
  GPIO134_PIN_CONTROL_REGISTER = 0x240UL; 
}


/* 1.52V is wired to ADC pin */
void vrefAdc152V(void)
{
  REF_SEL1 = 0;
  REF_SEL2 = 1;
}

/*3.3V is wired to ADC pin*/
void vrefAdc330V(void)
{
  REF_SEL1 = 1;
  REF_SEL2 = 0;
}

/* ADC pin is wired to ground */
void vrefAdcGnd(void)
{
  REF_SEL1 = 1;
  REF_SEL2 = 1;
}
void vrefAdcOpen(void)
{
  REF_SEL1 = 0;
  REF_SEL2 = 0;
}


