/*
* This file is a part of RMX HARDWARE WALLET
*
* This program is used exclusively under the belowmentioned license provided by 
* Microchip Technology Inc. and its subsidiaries and with the full compliance 
* with it. It is your responsibility to comply with these license terms:
* ***************************************************************************
* © 2018 Microchip Technology Inc. and its subsidiaries. 
* 
* Subject to your compliance with these terms, you may use Microchip software
* and any derivatives exclusively with Microchip products. It is your 
* responsibility to comply with third party license terms applicable to your 
* use of third party software (including open source software) that may 
* accompany Microchip software.
*   
* THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  
* NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, 
* INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, 
* AND FITNESS FOR A PARTICULAR PURPOSE. 
* 
* IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
* INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
* WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS 
* BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  
* TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
* CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
* OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
*****************************************************************************/

#include "irq.h"
#include "core_cm4.h"
#include "led.h"
#include "buttons.h"
#include "system_MEC170x.h"
//#include "uart.h"
//#include "systick.h"
#include "CEC1702_defines.h"


#include "buttons.h"

extern volatile bool action;

#define IRQ_VECTOR_ENABLE_SET_REGISTER    (*((volatile unsigned int*) 0x4000E200))
#define IRQ_VECTOR_ENABLE_CLEAR_REGISTER  (*((volatile unsigned int*) 0x4000E204))
#define BLOCK_IRQ_VECTOR_REGISTER         (*((volatile unsigned int*) 0x4000E208))

// ----- GIRQ registers to enable buttons signals to get through to their NVIC handler calls
#define GIRQ10_SOURCE_REGISTER            (*((volatile unsigned int*) 0x4000E028))
#define GIRQ10_ENABLE_SET_REGISTER        (*((volatile unsigned int*) 0x4000E02C))
#define GIRQ10_ENABLE_CLEAR_REGISTER      (*((volatile unsigned int*) 0x4000E034))
#define GIRQ10_RESULT_REGISTER            (*((volatile unsigned int*) 0x4000E030))

#define GIRQ11_SOURCE_REGISTER            (*((volatile unsigned int*) 0x4000E03C))
#define GIRQ11_ENABLE_SET_REGISTER        (*((volatile unsigned int*) 0x4000E040))
#define GIRQ11_ENABLE_CLEAR_REGISTER      (*((volatile unsigned int*) 0x4000E048))
#define GIRQ11_RESULT_REGISTER            (*((volatile unsigned int*) 0x4000E044))


/* Interrupts are not used in rmxwallet, but still letting the code here */

void GIRQ10_GIRQ11_init(void) {
  // Enabling interrupts at pin control registers
  GPIO027_PIN_CONTROL_REGISTER |= 0xE1UL;// 0xE1 - pullup enabled, falling edge triggered
  GPIO053_PIN_CONTROL_REGISTER |= 0xE1UL;// 0xE1 - pullup enabled, falling edge triggered
  GPIO054_PIN_CONTROL_REGISTER |= 0xE1UL;// 0xE1 - pullup enabled, falling edge triggered

  // Enabling GIRQ10 and GIRQ11 block
  IRQ_VECTOR_ENABLE_SET_REGISTER |= 0xC00;

  // Buttons
  // enabling COL3 (GPIO040) interrupt
  GIRQ10_ENABLE_SET_REGISTER     |= 0x01UL;
  // enabling COL1 (GPIO026) and COL2 (GPIO031) interrupt
  GIRQ11_ENABLE_SET_REGISTER     |= 0x2400000UL;

  // Other interrupts within GIRQ10 and GIRQ11 group:
  /*
  *
  */
  //GIRQ10_GIRQ11_enable();
}


void GIRQ10_GIRQ11_enable(void) {
  NVIC_EnableIRQ(GPIO_040_076_IRQn);
  NVIC_EnableIRQ(GPIO_000_036_IRQn);
  __DSB(); 
}


void GIRQ10_GIRQ11_disable(void) {
  NVIC_DisableIRQ(GPIO_040_076_IRQn);
  NVIC_DisableIRQ(GPIO_000_036_IRQn);
  __DSB();
}


void NVIC_Handler_GIRQ10(void) {
 ledBlink(SHORT);
}


void NVIC_Handler_GIRQ11(void) {
 ledBlink(SHORT);
}

  
  



