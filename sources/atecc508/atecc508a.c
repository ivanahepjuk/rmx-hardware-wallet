/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 872E 748B 97F7 8E39 8D61  706D 2A81 682B 4BE0 C6A5> <i_a@rmxwallet.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *   - Whereas this program also includes the software created under the copyright of Conor Patrick and Karl Koscher, 
 *     the following conditions are also part of the additional terms:
 *
 *     Copyright (c) 2016, Conor Patrick
 *     Copyright (c) 2016, Karl Koscher
 *     All rights reserved.
 *
 *     Redistribution and use in source and binary forms, with or without
 *     modification, are permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *     2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *     ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *     WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *     DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *     ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *     (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *     SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *     The views and conclusions contained in the software and documentation are those
 *     of the authors and should not be interpreted as representing official policies,
 *     either expressed or implied, of the FreeBSD Project.
 *      
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. Shall the abovementioned 
 * disclaimer provide stricter or different rule, it is considered additional term permitted
 * by the GNU General Public License and it prevails to the extent that it does not constitute
 * the further restriction according to section 10. of the GNU General Public License.
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */
 /*
 * atecc508.c
 * 		Implementation for ATECC508 peripheral.
 *
 */



#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "util.h"

#include "atecc508a.h"
#include "i2c.h"
#include "crc.h"
#include "util.h"
#include "util.h"
#include "MCHP_MEC170x.h"



void atecDumpConfig(void) {
  uint8_t config_bytes_one_block[1+32+2];//packet length + data + two byte checksum 
  uint16_t crc = 0;

  uartSend("\r\n\r\n       ATECC508A DUMP CONFIG:\r\n\r\n");
  i2cInit(1);
  atecc_wake();
  for (uint8_t i = 0; i<4; i++) {
    atecc_send(ATECC_CMD_READ, ATECC_RW_CONFIG | ATECC_RW_EXT, (uint16_t) i<<3, NULL, 0);
    wait(10);
    atecc_recv(config_bytes_one_block, sizeof(config_bytes_one_block));

    uartSend("packet length: ");
    uartSendChar_to_hex(config_bytes_one_block[0]);

    // printing out bytes and calculating checksum
    crc = feed_crc(crc,config_bytes_one_block[0]);
    for(uint32_t j = 0; j<32; j++) {
      crc = feed_crc(crc,config_bytes_one_block[j+1]);
      if((j)%8==0)
        uartSend("\r\n");
      uartSendChar_to_hex(config_bytes_one_block[j+1]); uartSendChar(' ');
    }

    uartSend("\r\n");
    uartSend("packet checksum:     ");
    uartSendChar_to_hex(config_bytes_one_block[33]);
    uartSendChar_to_hex(config_bytes_one_block[34]);
    uartSend("\r\n");
    uartSend("calculated checksum: ");
    crc = reverse_bits(crc);
    uartSend(tohexLE((uint8_t*) &crc, 2));
    uartSend("\r\n");
    wait(10);
    crc = 0;
  }
}



int8_t atecc_send(uint8_t cmd, uint8_t p1, uint16_t p2, uint8_t * buf, uint8_t len) {
  static uint8_t params[6];
  params[0] = 0x3;
  params[1] = 7+len;
  params[2] = cmd;
  params[3] = p1;
  params[4] = p2 & 0xff;
  params[5] = p2 >> 8;

  if (i2cSendExtPkt(ATECC508A_ADDR, params, sizeof(params), buf, len)) {
    return -1;
  }
  return 0;
}


int8_t atecc_recv(uint8_t * buf, uint8_t buflen) {
  uint8_t pkt_len;
  pkt_len = i2cRecvVariableLenPkt(ATECC508A_ADDR, buf, buflen);
  if (pkt_len == 0) {
    return -1;
  }
  return pkt_len;
}


void atecc_wake()
{
  uint8_t packet[] = {0x00, 0x00};
  i2cRecvPkt(0x7F, NULL, 0); // Resync I2C
  i2cSendPkt(ATECC508A_ADDR, packet, 2);
}

/*
// fahrplan:
atec INIT:


SlotConfog.ReadKey(ReadKeyID) - u slotu ktery chci takhle pouzivt nastavim key ID ktere ukaze na key kterym chci cist/psat
SlotConfog.EriteKey(ReadKeyID)   --/--/--/--
SlotConfig.EncryptRead
SlotConfig.IsSecret
slotConfig<14> musi byt 1 pro encrypted write


Pak zamknu config zonu: command Lock config

Vytvorim klic: vezmu pin, hashnu -> tohle bude read/write key.
Pak ho nahraju do slotu cislo viz.vyse
Zamknu data zonu - comand lock data zone

Pak 

(1) pres RNG vygeneruju nonce - tohle je salt v komunikaci

(2) poslu Nonce command: mode - 0x03(pass-through)
                         Zero = 0x0000
                         NumIn = Nonce

(3) atec si ulozi numin do TemkKey registru?

(4) poslu GenDig command: Zone  = Data (0x02)
                          KeyID = Write Key ID

Tohle vyvori v atec session key.

(5) Ted i u mne musim vytvorit session key, musim udelat ekvivalent tohohle:
    SHA-256
    Write Key, 
    GenDig Opcode (0x15),
    Zone (0x02), 
    Key ID, 
    SN[8],
    SN[0:1], 
    Zeros (25),
    Nonce

A mam Session Key


(6) tedka vezmu plaintext a xornu ho session key

(7) Spocitam host MAC:
SessionKey, Write opcode 0x02, param1 0x82, param2 address, sn[8], sn[0:1], zeros(25), plaintext -> host MAC
podle DS: (SHA-256(TempKey, Opcode, Param1, Param2, SN<8>, SN<0:1>, <25 bytes of zeros>, PlainTextData))
(8) poslu to write commandem:
Zone = Data(0x82)


*/
 






















#define CONFIG_BLOCK_0              0x0000
#define CONFIG_BLOCK_1              0x0008
#define CONFIG_BLOCK_2              0x0010
#define CONFIG_BLOCK_3              0x0018

#define CONFIG_BLOCK_OFFSET_0       0x0000
#define CONFIG_BLOCK_OFFSET_1       0x0001
#define CONFIG_BLOCK_OFFSET_2       0x0002
#define CONFIG_BLOCK_OFFSET_3       0x0003
#define CONFIG_BLOCK_OFFSET_4       0x0004
#define CONFIG_BLOCK_OFFSET_5       0x0005
#define CONFIG_BLOCK_OFFSET_6       0x0006
#define CONFIG_BLOCK_OFFSET_7       0x0007


int8_t atecc_setup_config() {
  //slotconfig 0-3
  uint8_t four_bytes[4] = {0,0,0,0};  
//slotconfig 0-15
  atecc_send(ATECC_CMD_WRITE, ATECC_RW_CONFIG, CONFIG_BLOCK_0 | CONFIG_BLOCK_OFFSET_7, four_bytes, 4); //
  wait(10);

//keyconfig 0-15
  atecc_send(ATECC_CMD_WRITE, ATECC_RW_CONFIG, CONFIG_BLOCK_3 | CONFIG_BLOCK_OFFSET_7, four_bytes, 4); //
  wait(10);




/*

  struct atecc_response res;
  uint8_t i;

  //struct atecc_slot_config sc;
  union slot_config sc;
  union key_config kc;

  uartSend("\r\n\r\n       ATECC508A SETUP BYTES TO BURN:\r\n\r\n");


  memset(&sc, 0, sizeof(union slot_config));
  memset(&kc, 0, sizeof(union key_config));
  sc.bits.readkey = 3;

  sc.bits.secret = 0;
  sc.bits.writeconfig = 0xa;
  uartSend("####### Slot config bytes:\r\n");
  // set up read/write permissions for keys
  for (i = 0; i < 16; i++) {
    uartSendChar_to_hex(sc.bytes[0]);
    uartSendChar_to_hex(sc.bytes[1]);
    uartSend(" ");
    if ( atecc_write_eeprom(ATECC_EEPROM_SLOT(i), ATECC_EEPROM_SLOT_OFFSET(i), sc.bytes, ATECC_EEPROM_SLOT_SIZE) != 0) 
      ;
      //u2f_printb("1 atecc_write_eeprom failed ",1, i);
      //return -1;
    
  }
  uartSend("\r\n");

  kc.bits.private = 0;
  kc.bits.pubinfo = 1;
  kc.bits.keytype = 0x4;
  kc.bits.lockable = 0;

  uartSend("####### Key's config bytes:\r\n");
  // set up config for keys
  for (i = 0; i < 16; i++) {
    uartSendChar_to_hex(kc.bytes[0]);
    uartSendChar_to_hex(kc.bytes[1]);
    uartSend(" ");
    
    if (i==15) {
      kc.bits.lockable = 1;
    }
    
    if ( atecc_write_eeprom(ATECC_EEPROM_KEY(i), ATECC_EEPROM_KEY_OFFSET(i), kc.bytes, ATECC_EEPROM_KEY_SIZE) != 0) {
      ;  //u2f_printb("3 atecc_write_eeprom failed " ,1,i);
      //return -2;
uartSend("fail ");
    }
  }
  
  uartSend("\r\n");
  //dump_config(buf);
  return 0;
*/
  return 0;
}

/*
int8_t atecc_write_eeprom(uint8_t base, uint8_t offset, uint8_t* srcbuf, uint8_t len) {
  uint8_t buf[7];
  //struct atecc_response res;

  uint8_t * dstbuf = srcbuf;
  if (offset + len > 4)
    return -1;
  if (len < 4) {
    atecc_send_recv(ATECC_CMD_READ,ATECC_RW_CONFIG, base, NULL, 0,buf, sizeof(buf));
    //dstbuf = res.buf;
    //memmove(res.buf + offset, srcbuf, len);
  }

  atecc_send_recv(ATECC_CMD_WRITE,ATECC_RW_CONFIG, base, dstbuf, 4,buf, sizeof(buf));
//  if (get_app_error()) {
//      return -1;
//  }
  return 0;
}

*/


/*
int8_t atecc_send_recv(uint8_t cmd, uint8_t p1, uint16_t p2, uint8_t* tx, uint8_t txlen, uint8_t * rx, uint8_t rxlen) {
  uint8_t errors = 0;

  //atecc_wake();
  wait(10);
  while(atecc_send(cmd, p1, p2, tx, txlen)) {
    wait(10);
    errors++;
    if (errors > 8) {
      return -1;
    }
  }
  wait(1);
  while(atecc_recv(rx,rxlen) == -1) {
    errors++;
    if (errors > 5)
    {
      return -2;
    }
  }
  //atecc_idle();
  return 0;
}



void atecc_idle()
{
  uint8_t packet[] = {0x02};
  i2cSendPkt(ATECC508A_ADDR, packet, 1);
}

void atecc_sleep()
{
  uint8_t packet[] = {0x01};
  i2cSendPkt(ATECC508A_ADDR, packet, 1);
}


*/

/*

static void delay_cmd(uint8_t cmd)
{
  uint8_t d = 0;
  switch(cmd) {
    case ATECC_CMD_SIGN:
      d = 50;
      break;
    case ATECC_CMD_GENKEY:
      d = 100;
      break;
    default:
      d = 32;
      break;
  }
  //u2f_delay(d);
  wait(d);
}

*/
/*
int is_locked(uint8_t * buf) {
//  struct atecc_response res;
  atecc_send_recv(ATECC_CMD_READ,ATECC_RW_CONFIG,87/4, NULL, 0,buf, 36);
  //dump_hex(res.buf, res.len);
  if (res.buf[87 % 4] == 0)
    return 1;
  else
    return 0;
}


*/


