/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "xmpp.h"
#include "util.h"
#include "uart.h"
#include "mec2016_rom_api.h"
#include "crypto.h"
#include "login.h"

#include <stdbool.h>
#include <stdio.h>
#include "stdint.h"
#include "communication.h"

#include <pb_encode.h>
#include <pb_decode.h>
#include "xmpp.pb.h"
#include "flash_memory.h"
#include "storage.h"


uint8_t buffer_out_xmpp[1024+128];
uint8_t buffer_in_xmpp [1024+128];

PAYLOAD Received = {0};
PAYLOAD To_send  = {0};

/*
//------------------------------------------------------------------------------------------------------//
void XMPPLogin(void) {
  bool status;//fixme define just once?
  uint32_t size_of_received_pb = 0;

  fillBuf(0x0000);
  animText(0, 10, "Trying to", 0xffff, 1);    
  animText(0, 20, "login..", 0xffff, 1);  

  OutXMPPLogin msg_out = OutXMPPLogin_init_zero;
  memcpy(msg_out.jid, xmpp_data[0].jid, 62);
  memcpy(msg_out.passwd, xmpp_data[0].metadata, 32);
  memcpy(msg_out.pubkey, xmpp_data[0].pubkey, 32);
  pb_ostream_t ostream = pb_ostream_from_buffer(buffer_out_xmpp, sizeof(buffer_out_xmpp));
  status = pb_encode(&ostream, OutXMPPLogin_fields, &msg_out);
  sendMessage(XMPP_LOGIN_OUT, ostream.bytes_written, buffer_out_xmpp);
  //message going in
  InXMPPLoginResponse msg_in = InXMPPLoginResponse_init_zero; 
  size_of_received_pb = receiveMessage(XMPP_LOGIN_IN, buffer_in_xmpp, TIMEOUT);
  pb_istream_t istream = pb_istream_from_buffer(buffer_in_xmpp, size_of_received_pb);
  status = pb_decode(&istream, InXMPPLoginResponse_fields, &msg_in);
  if((msg_in.status_code == InXMPPLoginResponse_StatusCode_OK) && status){   //ADD MODE CHECKING
    //return false; // successfully connected
    ledBlink(SHORT);
    fillBuf(0x0000);
    animText(0, 10, "Login", 0xffff, 1);    
    animText(0, 20, "OK", 0xffff, 1);  
    XMPPQueryUnread();
  }
}

//query for unread and get numbers of unread messages
void XMPPQueryUnread(void) {
  bool status;//fixme define just once?
  uint32_t size_of_received_pb = 0;
  OutQueryUnread msg_out = OutQueryUnread_init_zero;
  msg_out.dummy = 0x00;//??
  pb_ostream_t ostream = pb_ostream_from_buffer(buffer_out_xmpp, sizeof(buffer_out_xmpp));
  status = pb_encode(&ostream, OutQueryUnread_fields, &msg_out);
  sendMessage(XMPP_QUERY_UNREAD_OUT, ostream.bytes_written, buffer_out_xmpp);
  //message going in
  InQueryUnread msg_in = InQueryUnread_init_zero; 
  size_of_received_pb = receiveMessage(XMPP_QUERY_UNREAD_IN, buffer_in_xmpp, TIMEOUT);
  pb_istream_t istream = pb_istream_from_buffer(buffer_in_xmpp, size_of_received_pb);
  status = pb_decode(&istream, InQueryUnread_fields, &msg_in);
  if(status){   //ADD MODE CHECKING
    uint16_t index = 0;
    char jid[62] = {0};
    while((index<1024) && (msg_in.unread_nicks[index] != '\0')) {
      int i = 0;
      while(msg_in.unread_nicks[index] != ';') {
        jid[i] = msg_in.unread_nicks[index];
        index++;
        i++;
      }
      jid[i] = '\0';
      index++;
      updateUnread(jid, msg_in.unread_nicks[index]);
      index++;
      memzero((uint8_t*)jid, 62);
      ledBlink(SHORT);
    }
  }
}

void updateUnread(char str[], uint8_t unread) {
  //animText(5, 100, str, 0xffff, 1);

  uint8_t size = strlen(str);
  for(uint16_t i = 0; i<32; i++) {
    if(strncmp(xmpp_data[i].jid, str, size-1) == 0) {
      xmpp_data[i].unread = unread;
      //animText(0, 100, str, 0xffff, 1);
    }
  }
}



OutQueryMessage meesg_out = OutQueryMessage_init_zero;
void XMPPDownloadMessage(uint8_t index) {

  bool status;//fixme define just once?
  uint32_t size_of_received_pb = 0;

  
  memcpy(meesg_out.from_jid, xmpp_data[index].jid, 62);
  pb_ostream_t ostream = pb_ostream_from_buffer(buffer_out_xmpp, sizeof(buffer_out_xmpp));
  status = pb_encode(&ostream, OutQueryMessage_fields, &meesg_out);

  sendMessage(XMPP_QUERY_MESSAGE_OUT, ostream.bytes_written, buffer_out_xmpp);
  //message going in
  InMessage msg_in = InMessage_init_zero; 
  size_of_received_pb = receiveMessage(XMPP_QUERY_MESSAGE_IN, buffer_in_xmpp, TIMEOUT);
  pb_istream_t istream = pb_istream_from_buffer(buffer_in_xmpp, size_of_received_pb);
  status = pb_decode(&istream, InMessage_fields, &msg_in);

  if(status){

    fillBuf(0x0000);
    payloadZeroes(&Received);

//    for(int i = 0; i<62; i++)
//      Received.jid[i] = msg_in.jid[i];
//    for(int i = 0; i<256; i++)
//      Received.message[i] = msg_in.message[i];
//    for(int i = 0; i<32; i++)
//      Received.metadata[i] = msg_in.metadata[i];
//    for(int i = 0; i<32; i++)
//      Received.checksum[i] = msg_in.checksum[i];

    memcpy(Received.jid, msg_in.jid, 62);
    //memcpy((uint8_t*)Received.timestamp, (uint8_t*)msg_in.timestamp, 4);
    memcpy(Received.message, msg_in.message, 256);
    memcpy(Received.metadata, msg_in.metadata, 32);
    memcpy(Received.checksum, msg_in.checksum, 32);
    //test checksum
    decryptMessage();
    
    //payloadZeroes(&Received);
    drawStringZoom(53, 120, "[ok]", 0xffff, 1);
    writeBuf();
    waitForOK();
   
    fillBuf(0x0000);
    animText(0, 0, "Delete message", 0xffff, 1);
    animText(0, 10, "from server?", 0xffff, 1);
    drawStringZoom(0, 120, "YES", 0xffff, 1);
    drawStringZoom(100, 120, "NO", 0xffff, 1);
    writeBuf();
    int8_t key;
    while((key = scanAnyKey()) == -1);
    fillBuf(0x0000);
    if(key == 1) {
      //XMPPMessageAck(0);
      int size = strlen ((char*)&Received.jid);
      for(uint16_t i = 0; i<32; i++) {
        if(strncmp((char*)&xmpp_data[i].jid, (char*)&Received.jid, size-1) == 0) {
          xmpp_data[i].unread = xmpp_data[i].unread - 1;
        }
      }
      fillBuf(0x0000);
      animText(0, 0, "Deleted", 0xffff, 1);
    }
    if(key == 3) {
      //XMPPMessageAck(1);
      fillBuf(0x0000);
      animText(0, 0, "Not deleted", 0xffff, 1);
    }

  }
}

//delete last readed message from server? 0 YES, 1 NO
void XMPPMessageAck (uint8_t ack) {
  bool status;//fixme define just once?
 
  OutMessageAck msg_out = OutMessageAck_init_zero;

  msg_out.action = ack;

  pb_ostream_t ostream = pb_ostream_from_buffer(buffer_out_xmpp, sizeof(buffer_out_xmpp));
  status = pb_encode(&ostream, OutMessageAck_fields, &msg_out);
  sendMessage(XMPP_MESSAGE_DELETE_OUT, ostream.bytes_written, buffer_out_xmpp);
  if(status)
    ledBlink(SHORT);
}

//send out message data from To_send structure
void XMPPSendMessage(void) {
  bool status;//fixme define just once?
  uint32_t size_of_received_pb = 0;
  encryptMessage();
  OutMessage msg_out = OutMessage_init_zero;
  memcpy(msg_out.jid, To_send.jid, 62);
  memcpy(msg_out.metadata, To_send.metadata,32);
  memcpy(msg_out.message, To_send.message, 256);
  pb_ostream_t ostream = pb_ostream_from_buffer(buffer_out_xmpp, sizeof(buffer_out_xmpp));
  status = pb_encode(&ostream, OutMessage_fields, &msg_out);
  sendMessage(XMPP_MESSAGE_OUT, ostream.bytes_written, buffer_out_xmpp);
  //message going in
  OutMessageAck msg_in = InMessageAck_init_zero; 
  size_of_received_pb = receiveMessage(XMPP_MESSAGE_IN, buffer_in_xmpp, TIMEOUT);
  pb_istream_t istream = pb_istream_from_buffer(buffer_in_xmpp, size_of_received_pb);
  status = pb_decode(&istream, InMessageAck_fields, &msg_in);
  if(status){   
    ledBlink(SHORT);
    //test checksum
  }
}

//get message from keyboard, encrypt, fill To_send tructure
void encryptMessage(void) {
  key32_t random;
  key32_t metadata;
  key32_t encryption_key;
  while(memzero(aes_input, AES_BLOCK_SIZE));
  while(memzero(aes_output, AES_BLOCK_SIZE));
  char input[256];
  uint16_t input_size = 0;
  input_size = keyboard(input, 256, "Message:"); 
  memcpy(aes_input,input, input_size);
  rng_get_random_blocking((uint32_t*) random, KEYSIZE/4);
  scalarMult(metadata, random, &G, NO_COMPRESSION); 
  int contact = browseContacts();
  scalarMultKey(encryption_key, random, xmpp_data[contact].pubkey);
  keccak_256(encryption_key, KEYSIZE, encryption_key);
  memcpy(aes_key_array , encryption_key, 32);
  deriveAesIv(encryption_key, aes_init_vector);
  while(api_aes_busy());
  api_aes_set_key ((uint32_t*)aes_key_array, (uint32_t*)aes_init_vector, AES_KEYLEN_256, false);
  api_aes_crypt ((uint32_t*)aes_input, (uint32_t*)aes_output, 16, AES_MODE_CBC);//(uint8_t)0x02);//AES_MODE_CBC);
  api_aes_start(0);
  while(api_aes_busy());
  api_aes_stop();
  memcpy(To_send.jid, xmpp_data[contact].jid, 62);
  memcpy(To_send.message, aes_output, 256);
  memcpy(To_send.metadata, metadata, 32);
}

void XMPPAddContact(void) {
  bool status = false;//fixme define just once?
  uint32_t size_of_received_pb = 0;
  OutAddContact msg_out = OutAddContact_init_zero;
  char contact_to_add[62];
  memzero((uint8_t*)contact_to_add, 62);
  fillBuf(0x0000);
  keyboard(contact_to_add, 62, "Contact to add:");
  memcpy(msg_out.jid, contact_to_add, strlen(contact_to_add)); 
  pb_ostream_t ostream = pb_ostream_from_buffer(buffer_out_xmpp, sizeof(buffer_out_xmpp)); 
  status = pb_encode(&ostream, OutAddContact_fields, &msg_out);
  InAddContact msg_in = InAddContact_init_zero;
  //message going in
  sendMessage(XMPP_ADD_CONTACT_OUT, ostream.bytes_written, buffer_out_xmpp);
  size_of_received_pb = receiveMessage(XMPP_ADD_CONTACT_IN, buffer_in_xmpp, TIMEOUT);
  pb_istream_t istream = pb_istream_from_buffer(buffer_in_xmpp, size_of_received_pb);
  status = pb_decode(&istream, InAddContact_fields, &msg_in);
  if(status) {
    uint8_t downloaded_contact[94];
    memzero(downloaded_contact, 94);
    memcpy(downloaded_contact, msg_in.jid, 62);
    memcpy(downloaded_contact+62, msg_in.pubkey, 32);
    saveContact(downloaded_contact);
  }
  else {
    while(1) {
      ledBlink(SHORT);
      wait(300);
    }
  }
}

void decryptMessage(void) {
  key32_t decryption_key;
  // DECRYPTION KEY 
  // recovers metadata into coordinates, multiplies it with recipients privkey, store into decryption key 
  scalarMultKey(decryption_key, privkey, Received.metadata);
  // hash the result 
  keccak_256(decryption_key, KEYSIZE, decryption_key);
  // creating init vector from encryption key 
  deriveAesIv(decryption_key, aes_init_vector);
  // copying encryption key into aes_array 
  memcpy(aes_key_array, decryption_key, 32);
  // copying encrypted message into aes input field 
  memcpy(aes_input, Received.message, 256);
  // while(memzero(aes_output, AES_MEMORY_SIZE)); 
  while(api_aes_busy());
  api_aes_set_key ((uint32_t*)aes_key_array, (uint32_t*)aes_init_vector, AES_KEYLEN_256, false);
  api_aes_crypt ((uint32_t*)aes_input, (uint32_t*)aes_output, 16, AES_MODE_DECRYPT|AES_MODE_CBC);//
  api_aes_start(0);
  while(api_aes_busy());
  api_aes_stop();

  fillBuf(0x0000);
  drawMessage((char*)aes_output, 0xFFFF, 1);
  drawStringZoom(0,0, "Received message", 0xffff, 1);
  writeBuf();
}


void payloadZeroes(PAYLOAD* Structure) {
  memzero(Structure->jid, 62);
  memzero((uint8_t*)Structure->timestamp, 4);
  memzero(Structure->message, 32);
  memzero(Structure->metadata, 32);
  memzero(Structure->checksum, 32);
}

void XMPPReadMessagesMenu(void) {
  XMPPQueryUnread();
  int focus = 0;
  //int exit = 11;
  while (1) {
    //wait(0);
    int8_t pressed = scanAnyKey();
    debounce();
    switch(pressed) {
      case 1:
        focus--; 
        break;
      case 2:
        if(focus == 11) {
         return;
        }
        XMPPDownloadMessage(focus);
        break;//return;
      case 3:
        focus++; 
        break;
      default:
        break;
    }
    if (focus > 11)
      focus = 0;
    if (focus < 0)
      focus = 11;
    fillBuf(0x0000);    
    for(uint8_t i = 0; i<11; i++) {
      drawStringZoom(5, 4+(10*i), xmpp_data[i].jid, WHITE, 1);
      //displaying unread
      char helper [4];
      itoaa(xmpp_data[i].unread, helper);
      fillRect(109,3+(10*i), 19,9,WHITE);
      drawStringZoom(110, 4+(10*i), helper, BLACK, 1);
      if(focus == i) {
        fillRect(3, (2+(10*i)), 105, 12, RED);
        drawStringZoom(5, 4+(10*i), xmpp_data[i].jid, WHITE, 1);
      }
    }
    //exit
    drawStringZoom(5, 4+(10*11), "EXIT", WHITE, 1);
    if(focus == 11) {
      fillRect(3, (2+(10*11)), 105, 12, RED);
      drawStringZoom(5, 4+(10*11), "EXIT", WHITE, 1);
    }
    writeBuf();
  }
}



//////////////////////////////////////////////////

void XMPPLoginSetup(void) {
  char my_login[62];  
  char my_password[32];  
  memzero((uint8_t*)my_login, 62);
  memzero((uint8_t*)my_password, 32);
  keyboard(my_login, 32, "Setup login:"); 
  memcpy((uint8_t*)xmpp_data[0].jid, my_login, 62);
  keyboard(my_password, 32, "Setup password:");   
  memcpy((uint8_t*)xmpp_data[0].metadata, my_password, 32);
  memzero((uint8_t*)my_login, 32);
  memzero((uint8_t*)my_password, 32);
  scalarMult(xmpp_data[0].pubkey, privkey, &G, NO_COMPRESSION); 
  storageUpdate(0);
}



uint8_t browseContacts(void) {
  int8_t contact_pos = 0;
  bool update = true;
  bool setupDone = false;

  while (!setupDone) {
    switch(scanAnyKey()) {
      case 1:
        contact_pos--;
        update = true;
        debounce();
        break;
      case 2:
        setupDone = true;
        debounce();
        break;
      case 3:
        contact_pos++;
        update = true;
        debounce();
        break;
      default:
        break;
    }
    if(contact_pos < 0)
      contact_pos = 15;
    if(contact_pos > 15)
      contact_pos = 0;
    if(update) {
      if(contact_pos >= 0 && contact_pos <=15) {
        char number[5];
        itoaa((uint32_t)contact_pos+1, number);
        fillBuf(0x0000);
        placeNavigation(LEFTRIGHT);
        drawStringZoom(0,0, "Position", 0xffff, 1);
        drawStringZoom(43,0, number, 0xffff, 1);
        drawStringZoom(0,10, xmpp_data[contact_pos].jid, 0xffff, 1);
        showColors((uint8_t*)xmpp_data[contact_pos].pubkey);
        update = false;
      }
    }
  }
  return contact_pos;
}

void saveContact(uint8_t data[]) {
  uint8_t position = browseContacts();
  memcpy(xmpp_data[position].jid, data,     62);
  memcpy(xmpp_data[position].pubkey, data + 62,  32);
//  fillBuf(0x0000);
//  animText(0, 0, "Contact added.", 0xffff, 1);
//  wait(300);
  storageUpdate(0);
}

void deleteContact(void) {
  uint8_t position = browseContacts();
  memzero((uint8_t*)xmpp_data[position].jid, 62);
  memzero((uint8_t*)xmpp_data[position].pubkey, 32);
  memcpy(xmpp_data[position].jid, "<empty contact>", 16);
//  fillBuf(0x0000);
//  animText(0, 0, "Contact deleted.", 0xffff, 1);
//  wait(300);
  storageUpdate(0);
}
*/



