/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2019 m2049r@monerujo.io
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTS_H
#define TESTS_H

#include <stdint.h>
#include "crypto.h"

/** Switching the tests with defines from here **/
#define TEST_KEY_IMAGES   10


bool initTesting(uint32_t test_type);
bool testKeyImage (void);

#endif
