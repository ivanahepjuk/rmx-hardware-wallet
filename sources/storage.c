/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdint.h"
#include "stdbool.h"
#include "flash_memory.h"
#include "login.h"
#include "storage.h"
#include "ui-functions.h"
#include "communication.h"

/*
 * accelerator needs alligned accces, memory regions are defined here.
*/
__attribute__((section(".aes_input")))       uint8_t aes_input     [4096];
__attribute__((section(".aes_output")))      uint8_t aes_output    [4096];
__attribute__((section(".xmpp_data")))       CONTACT xmpp_data       [32] = {0};
__attribute__((section(".monero_data")))     MONERO_DATA monero_data[1];
__attribute__((section(".aes_key_array")))   uint8_t aes_key_array   [32];
__attribute__((section(".aes_init_vector"))) uint8_t aes_init_vector [32];
__attribute__((section(".privkey")))         uint8_t privkey         [32];      


/*
 * This is something like "factory reset", wipes out all metadata stored inside, except seed,
 * because seed is stored at different place.
*/
void initPersistentStorage (void) {
  login();
  //  wait(700);
  fillBuf(0x0000);
  drawStringZoom(0, 0, "Delete all XMPP data?", RED, 1);
  drawStringZoom(0, 120, "YES", 0xffff, 1);
  drawStringZoom(100, 120, "NO", 0xffff, 1);
  writeBuf();
  int key = 1;
  while((key = scanAnyKey()) == -1)
    ;
  fillBuf(0x0000);
  if(key == 1) {
    memzero((uint8_t*)xmpp_data, XMPP_STORAGE_SIZE);
    for(uint8_t i = 0; i<32; i++)
      xmpp_data[i].metadata[31] = 'g';
    for(uint8_t i = 0; i<32; i++)
      memcpy((uint8_t*)xmpp_data[i].jid, "<empty contact>", 16);
    memcpy(aes_input, xmpp_data, 4096);
    cryptBlock (ENCRYPT_BLOCK);
    spiEnable();
    while(saveData(aes_output, XMPP_MEMORY_ADDRESS));      
    animText(0, 0, "XMPP Init done", 0xffff, 1);
    wait(300);
  }
  if (key == 3) {
    animText(0, 0, "XMPP init canceled", 0xffff, 1);
    wait(300);
  }


  fillBuf(0x0000);
  drawStringZoom(0, 0, "Delete all XMR data?", RED, 1);
  drawStringZoom(0, 120, "YES", 0xffff, 1);
  drawStringZoom(100, 120, "NO", 0xffff, 1);
  writeBuf();
  key = 0;
  while((key = scanAnyKey()) == -1)
    ;
  fillBuf(0x0000);
  if(key == 1) {
    uint8_t *p_monero_data = (uint8_t*)&monero_data;
    memzero(p_monero_data, MONERO_STORAGE_SIZE);   
    memcpy(monero_data->setup.node_address,"127.0.0.1:38080", 16);
    monero_data->setup.local_blockheight  = 490400;//446000;//1901000;//446000;
    monero_data->setup.local_tx_offset = 0;
    monero_data->setup.batch_size = BATCH_SIZE;
    //monero_data->setup.matches = 0;
    monero_data->setup.balance = 0;
    monero_data->setup.global_blockheight = 0xffffffff;
    monero_data->wallet.savedBytes = 0;

    //saving block 1
    memcpy(aes_input, &p_monero_data[0], 4096);
    cryptBlock (ENCRYPT_BLOCK);
    spiEnable();
    while(saveData(aes_output, MONERO_MEMORY_ADDRESS))
      ;     
    //saving block 2
    memcpy(aes_input, &p_monero_data[AES_BLOCK_SIZE], 4096);
    cryptBlock (ENCRYPT_BLOCK);
    while(saveData(aes_output, MONERO_MEMORY_ADDRESS+AES_BLOCK_SIZE))
      ;   
    animText(0, 0, "XMR Init done", 0xffff, 1);
    wait(300);
  }
  if (key == 3) {
    animText(0, 0, "XMR init canceled", 0xffff, 1);
    wait(300);
  }
    spiDisable();
}


/* 
 * direction: TRUE to encrypt, FALSE to decrypt
 * #define DECRYPT_BLOCK       0
 * #define ENCRYPT_BLOCK       1
*/
bool cryptBlock (uint8_t direction) {
  while(api_aes_busy());
  api_aes_set_key ((uint32_t*)privkey, (uint32_t*)aes_init_vector, AES_KEYLEN_256, false);
  if(direction) {
    api_aes_crypt ((uint32_t*)aes_input, (uint32_t*)aes_output, 256, AES_MODE_CBC);
  } else {
    api_aes_crypt ((uint32_t*)aes_input, (uint32_t*)aes_output, 256, AES_MODE_CBC|AES_MODE_DECRYPT);
  }
  api_aes_start(0);
  while(api_aes_busy());
  api_aes_stop();

  return false;
}

/*
 * Encrypt all live data and save them into flash in encrypted form
*/
void storageUpdate(uint8_t block) {
  spiEnable();
  resetFlash();
  if(block == 0) {
    memcpy(aes_input, xmpp_data, AES_BLOCK_SIZE);
    cryptBlock(ENCRYPT_BLOCK);
    while(saveData(aes_output, XMPP_MEMORY_ADDRESS));
  }
  if(block == 1) {
    uint8_t *p_monero_data = (uint8_t*)&monero_data;
    //saving block 1
    memcpy(aes_input, &p_monero_data[0], AES_BLOCK_SIZE);
    cryptBlock (ENCRYPT_BLOCK);
    while(saveData(aes_output, MONERO_MEMORY_ADDRESS))
      ;     
    //saving block 2
    memcpy(aes_input, &p_monero_data[AES_BLOCK_SIZE], AES_BLOCK_SIZE);
    cryptBlock (ENCRYPT_BLOCK);
    while(saveData(aes_output, MONERO_MEMORY_ADDRESS+AES_BLOCK_SIZE))
      ;   
  }
  spiDisable();
}


/* 
 * This tests lasts around 11 minutes, based on SPI frequency */

/*
void testStorage() {
  while(login())
    wait(700);
  fillBuf(0x0000);
  animText(0, 0, "Testing the flash", 0xffff, 1);
  memzero((uint8_t*)xmpp_data, STORAGE_BLOCK_SIZE);
  for(uint8_t i = 0; i<32; i++)
    xmpp_data[i].metadata[31] = 'g';
  for(uint8_t i = 0; i<32; i++)
    memcpy((uint8_t*)xmpp_data[i].jid, "<empty contact>", 16);
  memcpy(aes_input, xmpp_data, 4096);
  cryptBlock (ENCRYPT_BLOCK);
  spiEnable();

  uint16_t checked = 0;
  uint16_t fucked = 0;

  for(uint16_t i = 0; i<1000;i++) {
    bool status = saveData(aes_output, XMPP_MEMORY_ADDRESS); 
    if(status)
      checked++;
    if(!status) 
      fucked++;
  }
  spiDisable();
  uartSend("Writing 4kB to flash, reading and checking (10000x): \r\n");
  char helper[8];
  itoaa(checked, helper);
  uartSend("readed OK: ");
  uartSend(helper);
  uartSend(" fucked: ");
  itoaa(fucked, helper);
  uartSend(helper);
  uartSend("\r\n");
}
*/























