/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 872E 748B 97F7 8E39 8D61  706D 2A81 682B 4BE0 C6A5> <i_a@rmxwallet.org>
 * Copyright (C) m2049r <m2049r@monerujo.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *   - This program also includes the software created under the copyright of Aleksey Kravchenko 
 *     <rhash.admin@gmail.com> (2013) et al.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * This program includes sha3.h - an implementation of Secure Hash Algorithm 3 (Keccak)
 * based on The Keccak SHA-3 submission. Submission to NIST (Round 3), 2011 by Guido Bertoni, Joan Daemen, Michaël Peeters and Gilles Van      
 * Assche
 */

#include <stddef.h>
#include <string.h>
#include "monero_others.h"
#include "crypto.h"
#include "sha3.h"
#include "util.h"



void scanningTest(void) {
/*  
  Objective: We want proceed and test scanning routines. Let's take known Tx, determine which output is ours, decrypt the amount, create spend key.
  Test preparation: We have to prepare known wallet and put some XMR on it. 

  [1] Creating a wallet
  Wallet
    viewkey 
    secret: 0a6d8b37a896baea2f411843fd9e1184cb2f6c195fa57cb6a6df81b135cf6b01
    //public: c2ee99336c91264753e24a7b74f3df7ee9064bd14b8e736aea69fcbe329d15c2
    spendkey
    secret: 6bcb164908642918b660096529f92c40b22a33ae3d0bf73c594c0b21039eba00
    //public: a89aceb8e84b2bf3f87655ad9a9a87e286d3deac692f762be851c272ae007474

  [2] Funding a wallet
    TxID        f6772e074d5daef91d0050cbb79178d42b81888b36fd7f421711bcedbb683b58  
    blockheight: 473249
    amount      12.345000000000 
    R:           530cb04486095646f8bf00e4de29dfb038f3a9d0fd6fc288c7887e32271186a2 (transaction's public key R)
    P[0]:        dbf2922ecf49468ab09cf70fc4294de5036f2befaaaa14e0a108a0f5dd13128a (Output P, index 0)
    P[1]:        a6d3591d0deb636123f570ef982f1f700b95e53f71178564f4d954fd821be58a (Output P, index 1)
    amount[0]:   da2417d28d76dfef
    amount[1]:   21b6d38badf7b862

  [3] checking key image - this wallet wass all sweapt out in TxID ed644d6b3d22df23c697b68a08b2b189b93a6c2e6d960fb7ae10d2c3e4ce3a84
 
now we are ready to test.
      Initializing variables with data used for scanning:
*/
  /** Initializing wallet's secret view key: 
  hitched daytime knapsack jerseys sensible tissue 
  cottage fudge refer large rest western bogeys 
  mirror drowning nitrogen jester pride hinder 
  asked diet opposite eject eleven hinder 
  **/
  key32_t a;
  memcpy(a, fromhexLE("0a6d8b37a896baea2f411843fd9e1184cb2f6c195fa57cb6a6df81b135cf6b01"), KEYSIZE);
  /** Initializing wallet's secret spend key: **/
  key32_t b;
  memcpy(b, fromhexLE("6bcb164908642918b660096529f92c40b22a33ae3d0bf73c594c0b21039eba00"), KEYSIZE);
  /** Initializing transaction's Public key: **/
  key32_t R;
  memcpy(R, fromhexLE("530cb04486095646f8bf00e4de29dfb038f3a9d0fd6fc288c7887e32271186a2"), KEYSIZE);
  /** Initializing two wallet's outputs: **/
  key32_t P[2];
  memcpy(P[0], fromhexLE("dbf2922ecf49468ab09cf70fc4294de5036f2befaaaa14e0a108a0f5dd13128a"), KEYSIZE);
  memcpy(P[1], fromhexLE("a6d3591d0deb636123f570ef982f1f700b95e53f71178564f4d954fd821be58a"), KEYSIZE);
  /** Initializing two wallet's amounts: **/
  uint8_t amount[2][8];
  memcpy(amount[0], fromhexLE("da2417d28d76dfef"), 8);
  memcpy(amount[1], fromhexLE("21b6d38badf7b862"), 8);
  /** Initializing TxID - useless for computing, but maybe helps somebody to track this example on stagenet's blockchain**/
  scalar32_t TxID;
  memcpy(TxID, fromhexLE("f6772e074d5daef91d0050cbb79178d42b81888b36fd7f421711bcedbb683b58"), KEYSIZE);

  /** Initializing variables used during processing the data **/
  scalar32_t eight_a; 
  key32_t D;  
  /** Scanning routine starts here **/

/** Print out all wallet data **/  
  uartSend("\r\nWallet data:");
  uartSend("\r\nspendkey: "); uartSend(tohexLE(b, SCALARSIZE));
  uartSend("\r\nviewkey:  "); uartSend(tohexLE(a, SCALARSIZE));
/** Print out all transaction's data **/
  uartSend("\r\n\r\nTransaction's data:");
  uartSend("\r\nTx ID:    "); uartSend(tohexLE(TxID, SCALARSIZE));
  uartSend("\r\nR:        "); uartSend(tohexLE(R, SCALARSIZE));
  uartSend("\r\nP 0:      "); uartSend(tohexLE(P[0], SCALARSIZE));
  uartSend("\r\nP 1:      "); uartSend(tohexLE(P[1], SCALARSIZE));
  uartSend("\r\nAmount 0: "); uartSend(tohexLE(amount[0], AMOUNTSIZE));
  uartSend("\r\nAmount 1: "); uartSend(tohexLE(amount[1], AMOUNTSIZE));

  //   compute D = a*R
  //   compute P = Hs(D || i)*G + B
  //   compute x = Hs(D || i) + b      (and check if P==x*G)

  /*
   * P' = H(aR)G + B = H(aR)G + bG = (H(aR)+b)G
   */

  //   compute D = 8*a*R

/** At the beginning we precompute D **/
  mult8_modp(eight_a, a);
  scalarMultKey(D, eight_a, R);
  uartSend("\r\n\r\nPrecomputing 8a and D for this transaction: ");
  uartSend("\r\n8a:       "); uartSend(tohexLE(eight_a, SCALARSIZE));
  uartSend("\r\nD :       "); uartSend(tohexLE(D, KEYSIZE));

  uartSend("\r\n\r\nComputing match for all outputs:");
  for(size_t output_index = 0; output_index<2; output_index++) {
    uartSend("\r\n  Index n.");uartNum(output_index);//uartSend("\r\n");
    /* H(D||i) */
    scalar32_t Hs_D;
    derivation_to_scalar(D, output_index, Hs_D);
    uartSend("\r\nHs(D):    "); uartSend(tohexLE(Hs_D, KEYSIZE));

    /* x := H(D||i)+b */
    scalar32_t x;
    add_modp(x, Hs_D, b);
    uartSend("\r\nx:        "); uartSend(tohexLE(x, SCALARSIZE));

    /* P := (H(aR||i)+b)G */
    key32_t Pd;
    scalarMultBase(Pd, x);
    uartSend("\r\nPd:       "); uartSend(tohexLE(Pd, KEYSIZE));

    /* Checking Pd against all known outputs, if match is found, output belongs to us */
    if (0 == memcmp(P[output_index], Pd, 32)) { 
      /* Match was found! Now compute key image and decrypt the amount*/
      uartSend("\r\nMatch found, keyImage for this output:");
      // keyImage = x(Hp(P))
      key32_t point;
      hash_to_ec(P[output_index], point);

      scalar32_t I;
      generate_key_image(point, x, I);

     // scalar32_t I;
      //keyImage(x, P[output_index], I);
      uartSend("\r\n--------keyImage: "); uartSend(tohexLE(I, SCALARSIZE));
      uartSend("\r\n--------expected: 40c13470bce4d81ab1d7a38d78e0d73daee6f26d206891bbe6bf9efedabc1a62");

      // decrypting amount here:
      scalar32_t ecdh;
      ecdhHash(Hs_D, ecdh);
      xor8(ecdh, &amount[output_index][0]);
      uartSend("\r\ndecrypted amount: "); uartSend(tohexLE((uint8_t*)&amount[output_index][0], 8));
      
      // making money out of that
      char money[33];
      amountToMoney(&amount[output_index][0], money);
      
      uartSend(" money: "); uartSend(money);
      uartSend(" XMR");
    } else {
      uartSend("\r\n...Match not found under this output's index");
    }
  }
}





