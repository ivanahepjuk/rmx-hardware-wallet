/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 872E 748B 97F7 8E39 8D61  706D 2A81 682B 4BE0 C6A5> <i_a@rmxwallet.org>
 * Copyright (C) m2049r <m2049r@monerujo.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *   - This program also includes the software created under the copyright of Aleksey Kravchenko 
 *     <rhash.admin@gmail.com> (2013) et al.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * This program includes sha3.h - an implementation of Secure Hash Algorithm 3 (Keccak)
 * based on The Keccak SHA-3 submission. Submission to NIST (Round 3), 2011 by Guido Bertoni, Joan Daemen, Michaël Peeters and Gilles Van      
 * Assche
 */

#include <stddef.h>
#include "monero_login.h"
#include "stdint.h"
#include <string.h>
#include <sys/types.h>
#include "crypto.h"
#include "sha3.h"


const char b58digits[] = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";

void encode58_block(char *b58, size_t size, const uint8_t *data, size_t datasz) {
	uint8_t buf[size];
	memset(buf, 0, size);

	for (ssize_t i = 0, high = size - 1, j = 0; i < (ssize_t)datasz; ++i, high = j) {
    j = size - 1;
		for (int carry = data[i]; (j > high) || carry; --j) {
			carry += 256 * buf[j];
			buf[j] = carry % 58;
			carry /= 58;
		}
	}

	for (size_t i = 0; i < size; ++i)
		b58[i] = b58digits[buf[i]];
}

#define FULL_BLOCKS 8
#define FULL_BLOCK_SIZE 8
#define REST_BLOCK_SIZE 5
#define FULL_ENCODED_BLOCK_SIZE 11
#define REST_ENCODED_BLOCK_SIZE 7

void encode58(char* res, const uint8_t* data) {
  // res must be 96 byte (95 characters + \0)
  // data must be 8*8+5 = 69 bytes public address for monero
  for (ssize_t i = 0; i < 8; i++)
    encode58_block(res + i * FULL_ENCODED_BLOCK_SIZE, FULL_ENCODED_BLOCK_SIZE, data + i * FULL_BLOCK_SIZE, FULL_BLOCK_SIZE);
  encode58_block(res + FULL_BLOCKS * FULL_ENCODED_BLOCK_SIZE, REST_ENCODED_BLOCK_SIZE, data + FULL_BLOCKS * FULL_BLOCK_SIZE, REST_BLOCK_SIZE);
  res[FULL_BLOCKS * FULL_ENCODED_BLOCK_SIZE + REST_ENCODED_BLOCK_SIZE] = 0;
}



void generateWalletAddress(uint8_t *address, uint8_t *privkey) {
  // computing the address from privkey:
  key32_t spendkey;
  memcpy(spendkey, privkey, KEYSIZE);
  reduce32(spendkey);
  key32_t viewkey;
  hash_to_scalar(spendkey, KEYSIZE, viewkey);
  // make public spend key
  key32_t pubSpendkey;
  scalarMultBase(pubSpendkey, spendkey);
  // make public view key
  key32_t pubViewkey;
  scalarMultBase(pubViewkey, viewkey);

  uint8_t addr[PUBSIZE];
  //address[0] = 18; // mainnet
  addr[0] = 24; // stagenet
  memcpy(addr + 1, pubSpendkey, KEYSIZE);
  memcpy(addr + 1 + KEYSIZE, pubViewkey, KEYSIZE);
  uint8_t hash[KEYSIZE];
  keccak_256(addr, 2 * KEYSIZE + 1, hash);
  memcpy(addr + 1 + 2 * KEYSIZE, hash, 4);
  // base58 it
  char public_address[96];
  encode58(public_address, addr);

  memcpy(address, public_address, 96);
}
