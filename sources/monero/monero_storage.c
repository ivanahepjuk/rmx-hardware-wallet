/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 872E 748B 97F7 8E39 8D61  706D 2A81 682B 4BE0 C6A5> <i_a@rmxwallet.org>
 * Copyright (C) m2049r <m2049r@monerujo.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *   - This program also includes the software created under the copyright of Aleksey Kravchenko 
 *     <rhash.admin@gmail.com> (2013) et al.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * This program includes sha3.h - an implementation of Secure Hash Algorithm 3 (Keccak)
 * based on The Keccak SHA-3 submission. Submission to NIST (Round 3), 2011 by Guido Bertoni, Joan Daemen, Michaël Peeters and Gilles Van      
 * Assche
 */

#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "monero_storage.h"
#include "uart.h"
#include "util.h"
#include "storage.h"
#include "ui-functions.h"
#include "buttons.h"



extern MONERO_DATA monero_data[];


bool saveTxID(uint8_t *TxID) {
  //if tx is already saved, return 0
  if(monero_data->wallet.savedBytes < TXID_STORAGE_SIZE) {//we have still space for storing something
    memcpy((uint8_t*)&monero_data->wallet.TxImages[monero_data->wallet.savedBytes], TxID, KEYSIZE);
    monero_data->wallet.savedBytes += KEYSIZE + 8;//fixme 8 define
    return 0;
  } else {
    return 1;
  }
}


bool saveKeyImage(uint8_t key_image[]) {
  if(monero_data->wallet.savedBytes < TXID_STORAGE_SIZE) {//we have still space for storing something
    memcpy((uint8_t*)&monero_data->wallet.TxImages[monero_data->wallet.savedBytes], key_image, KEYSIZE);
    monero_data->wallet.savedBytes += KEYSIZE;
    return 0;
  } else {
    return 1;
  }
}


bool saveAmount(uint8_t amount[]) {
  if(monero_data->wallet.savedBytes < TXID_STORAGE_SIZE) {//we have still space for storing something
    memcpy((uint8_t*)&monero_data->wallet.TxImages[monero_data->wallet.savedBytes], amount, AMOUNTSIZE);
    monero_data->wallet.savedBytes += AMOUNTSIZE;
    return 0;
  } else {
    errorMessage("No space in persistence storage");
    return 1;
  }
}


bool isTxSaved(uint8_t TxID[]) {
 for(uint32_t i = 0; i<monero_data->wallet.savedBytes; i=i+40) {
    if (0 == memcmp((uint8_t*)&monero_data->wallet.TxImages[i], TxID, 32)) {
      return 1;
    } 
  }
  return 0;
}





void dumpTxStorage (void) {
  uartSend("\r\nDump storage first roundd: \r\n");
  for(int i=0;i<32;i++) {
    uartSend(tohexLE((uint8_t*)&monero_data->wallet.TxImages[i*40], KEYSIZE));
    uartSend("  ");
    uartSend(tohexBE((uint8_t*)&monero_data->wallet.TxImages[i*40 + 32], AMOUNTSIZE));
    uartSend("\r\n");
  }
}


bool setupBlockheight(void) {
  char input[32];
  uint32_t number;

  memzero((uint8_t*)input, 32);
  keyboard(input, 32, "Scan from blockheight:"); 
  number = aatoi(input);
//  if(number < 0 || number > 0xffffffff)
//    return 1;
  monero_data->setup.local_blockheight = number;
  monero_data->setup.local_tx_offset = 0;
  //monero_data->setup.matches = 0;//
  storageUpdate(1);
  return 0;
}


bool setupNode(void) {
  char input[128];
  memzero((uint8_t*)input, 128);
  keyboard(input, 128, "Node address:"); 
  memcpy(monero_data->setup.node_address, input, 128);
/*
  monero_data->setup.local_blockheight = number;
  monero_data->setup.local_tx_offset = 0;\
  monero_data->setup.matches = 0;//
*/
  uartSend(monero_data->setup.node_address);
  storageUpdate(1);
  return 0;
}





