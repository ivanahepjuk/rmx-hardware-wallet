/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UART_H
#define UART_H

#include <stdbool.h>
#include "stdint.h" 
#include "util.h"
#include "led.h"
#include "display.h"

#define UART_CONFIGURATION_SELECT_REGISTER  (*((volatile uint8_t*) 0x400F27F0))
#define UART_LINE_CONTROL_REGISTER          (*((volatile uint8_t*) 0x400F2403))
#define UART_BAUDRATE_REGISTER_LSB          (*((volatile uint8_t*) 0x400F2400))
#define UART_ACTIVATE_REGISTER              (*((volatile uint8_t*) 0x400F2730))
#define UART_LINE_STATUS_REGISTER           (*((volatile uint8_t*) 0x400F2405))
#define UART_TRANSMIT_BUFFER_REGISTER       (*((volatile uint8_t*) 0x400F2400))
#define UART_RECEIVE_BUFFER_REGISTER        (*((volatile uint8_t*) 0x400F2400))


/** Initialize uart port 
 [GPIO's must be already initialized with init()] 
**/
void uart_init(void);


/** Sends one char **/
void uartSendChar(char send);


/** Send a char in a hexadecimal representation **/
void uartSendChar_to_hex(char send);


/** sends byte **/
void uartSendByte(uint8_t send);


/**  Sends string **/
void uartSend(const char *string);


/** Receive char **/
uint8_t uartReceiveChar(uint32_t timeout);



#endif
