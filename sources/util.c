/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include "util.h"
#include "mec2016_rom_api.h"

//because of seed colours
#include "crypto.h"
#include "buttons.h"
#include "login.h"


void apiVersion(void) {
  uartSend("\r\nROM API VERSION : ");
  uint32_t version = api_rom_ver();
  uartSend(tohexLE((uint8_t*)&version, 4));
}


size_t strlenX(const char* str) {
  size_t i;
  for (i = 0; str[i] != 0; ++i);
  return i+1;
}


#define FROMHEX_MAXLEN 512
const uint8_t *fromhexBE(const char *str) {
  static uint8_t buf[FROMHEX_MAXLEN];
  size_t len = strlenX(str) / 2;
  if (len > FROMHEX_MAXLEN) len = FROMHEX_MAXLEN;
  for (size_t i = 0; i < len; i++) {
    int c = 0;
    if (str[i * 2] >= '0' && str[i*2] <= '9') c += (str[i * 2] - '0') << 4;
    if ((str[i * 2] & ~0x20) >= 'A' && (str[i*2] & ~0x20) <= 'F') c += (10 + (str[i * 2] & ~0x20) - 'A') << 4;
    if (str[i * 2 + 1] >= '0' && str[i * 2 + 1] <= '9') c += (str[i * 2 + 1] - '0');
    if ((str[i * 2 + 1] & ~0x20) >= 'A' && (str[i * 2 + 1] & ~0x20) <= 'F') c += (10 + (str[i * 2 + 1] & ~0x20) - 'A');
    buf[len-i-1] = (uint8_t) c;
  }
return buf;
}


#define TOHEX_MAXLEN (2*FROMHEX_MAXLEN)
const char *tohexBE(const uint8_t * in, size_t inlen) {
  static char buf[TOHEX_MAXLEN+1];
  const char * hex = "0123456789abcdef";
  size_t len = inlen * 2;
  if (len > TOHEX_MAXLEN) len = TOHEX_MAXLEN;
    for (size_t i = 0; i < len/2; i++) {
    buf[len-1-2*i-1] = hex[(in[i]>>4) & 0xF];
    buf[len-1-2*i-0] = hex[ in[i]     & 0xF];
  }
  buf[len] = 0;
  return buf;
}


const uint8_t *fromhexLE(const char *str) {
  static uint8_t buf[FROMHEX_MAXLEN];
  size_t len = strlenX(str) / 2;
  if (len > FROMHEX_MAXLEN) len = FROMHEX_MAXLEN;
  for (size_t i = 0; i < len; i++) {
    int c = 0;
    if (str[i * 2] >= '0' && str[i*2] <= '9') c += (str[i * 2] - '0') << 4;
    if ((str[i * 2] & ~0x20) >= 'A' && (str[i*2] & ~0x20) <= 'F') c += (10 + (str[i * 2] & ~0x20) - 'A') << 4;
    if (str[i * 2 + 1] >= '0' && str[i * 2 + 1] <= '9') c += (str[i * 2 + 1] - '0');
    if ((str[i * 2 + 1] & ~0x20) >= 'A' && (str[i * 2 + 1] & ~0x20) <= 'F') c += (10 + (str[i * 2 + 1] & ~0x20) - 'A');
    buf[i] = (uint8_t) c;
  }
return buf;
}


const char *tohexLE(const uint8_t * in, size_t inlen) {
  static char buf[TOHEX_MAXLEN+1];
  const char * hex = "0123456789abcdef";
  size_t len = inlen * 2;
  if (len > TOHEX_MAXLEN) len = TOHEX_MAXLEN;
  for (size_t i = 0; i < len/2; i++) {
    buf[2*i+0] = hex[(in[i]>>4) & 0xF];
    buf[2*i+1] = hex[ in[i]     & 0xF];
  }
buf[len] = 0;
return buf;
}


// CHECKME FIXME 32b versus 64b 
char* itoaa(uint64_t i, char b[]){
    char const digit[] = "0123456789";
    char* p = b;
  //  if(i<0){
    //    *p++ = '-';
    //    i *= -1;
    //}
    uint64_t shifter = i;
    do{ // Move to where representation ends
        ++p;
        shifter = shifter/10;
    }while(shifter);
    *p = '\0';
    do{ // Move back, inserting digits as u go
        *--p = digit[i%10];
        i = i/10;
    }while(i);
    return b;
}

//CHECKME FIXME
int32_t aatoi(char* str) { 
    int res = 0;
    for (int i = 0; str[i] != '\0'; ++i) 
        res = res * 10 + str[i] - '0'; 
    return res; 
} 


void wait(int msec) {
  for (int i = 0; i<(msec*3200); i++)
    asm("mov r0,r0");
}


inline void wait_us(int usec) {
  for (int i = 0; i<usec; i++)
    asm("mov r0,r0");
}


char hexDigit(unsigned n) {
  if (n < 10) {
    return n + '0';
  } else {
      return (n - 10) + 'A';
    }
}


void charToHex(char c, char hex[3]) {
  hex[0] = hexDigit(c / 0x10);
  hex[1] = hexDigit(c % 0x10);
  hex[2] = '\0';
}


void showColors(uint8_t key[]) {
 
  int32_t sin[16] = {0, 383, 707, 924, 1000, 924, 707, 383, 0, -383, -707, -924, -1000, -924, -707, -383};
  int32_t cos[16] = {1000, 924, 707, 383, 0, -383, -707, -924, -1000, -924, -707, -383, 0, 383, 707, 924};
  int16_t x, y;
  uint16_t color;
  for(uint8_t i = 0; i<32; i++) {
    uint8_t angle = key[i] & 0x0F;
    uint8_t length = key[i] >> 4;
    x = length*cos[angle];
    x = x/210;
    y = length*sin[angle];
    y = y/210;
    color = (key[i]<<8)|key[i];
    drawLine(63, 63, 63+x, 63+y, color);
    drawLine(64, 64, 64+x, 64+y, color);
    drawLine(65, 65, 65+x, 65+y, color);
  }

  fillRect(0, 85, 110, 12, 0x0000);
  fillRect(0, 96, 110, 12, 0x0000);
  fillRect(0, 107, 110, 12, 0x0000);  //fillRect(0, 117, 110, 12, 0x0000);

  drawStringZoom(0,  86, tohexLE((uint8_t*)key   , 11), 0xffff, 1);
  drawStringZoom(0,  97, tohexLE((uint8_t*)key+11 , 11), 0xffff, 1);
  drawStringZoom(0, 108, tohexLE((uint8_t*)key+22, 10), 0xffff, 1);

  writeBuf();
}


bool memzero(uint8_t * array, uint32_t length) {
  for(uint32_t i = 0; i<length; i++) {
    array[i] = 0;
  }
  for(uint32_t i = 0; i<length; i++) {
    if (array[i] != 0)
      return true;
  }
  return false;
}


void uartNum(uint64_t num) {
  char converted[21];
  itoaa(num, converted);
  uartSend(converted);
}


/*
void copyRomToSram(void) {
  static union {
    uint32_t i;
    char b[4];
  } nums;
  
  volatile unsigned int *p = (unsigned int)0x00000000;//the beginning of the ROM memory
  volatile unsigned int *p2= (unsigned int)0x000B0000;
  uint32_t index = 0;

  //uartSend("Copying ROM address 0x000000 - 0x0000FFFF to SRAM 0x000B000 - 0x000BFFFF\r\n");

  while (index<16384) { //0xffff in 32bit words is 16384
    p2[index] = p[index];
    index ++;
  }

  uartSend("Copying DONE\r\n\r\n");
}
*/
