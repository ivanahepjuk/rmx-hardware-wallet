/*
* This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "uart.h"
#include "MCHP_MEC170x.h"
#include "communication.h"
#include "util.h"
#include "menus.h"
#include "fifo.h"
#include "CEC1702_defines.h"

#define FIFO_PC_1_OUTPUT   0x241UL
#define FIFO_PC_1_INPUT    0x041UL
#define FIFO_PC_2   0x01UL

// BIT BAND SETTINGS:
// DATA PINS OUTPUTS
#define D0_OUTPUT_BIT   (*((volatile uint8_t*) ((((((GPIO105_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))
#define D1_OUTPUT_BIT   (*((volatile uint8_t*) ((((((GPIO104_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))
#define D2_OUTPUT_BIT   (*((volatile uint8_t*) ((((((GPIO155_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))
#define D3_OUTPUT_BIT   (*((volatile uint8_t*) ((((((GPIO010_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))
#define D4_OUTPUT_BIT   (*((volatile uint8_t*) ((((((GPIO047_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))
#define D5_OUTPUT_BIT   (*((volatile uint8_t*) ((((((GPIO154_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))
#define D6_OUTPUT_BIT   (*((volatile uint8_t*) ((((((GPIO157_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))
#define D7_OUTPUT_BIT   (*((volatile uint8_t*) ((((((GPIO125_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))
// DATA PINS INPUTS
#define D0_INPUT_BIT    (*((volatile uint8_t*) ((((((GPIO105_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(24*4)+0x42000000) ))))
#define D1_INPUT_BIT    (*((volatile uint8_t*) ((((((GPIO104_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(24*4)+0x42000000) ))))
#define D2_INPUT_BIT    (*((volatile uint8_t*) ((((((GPIO155_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(24*4)+0x42000000) ))))
#define D3_INPUT_BIT    (*((volatile uint8_t*) ((((((GPIO010_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(24*4)+0x42000000) ))))
#define D4_INPUT_BIT    (*((volatile uint8_t*) ((((((GPIO047_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(24*4)+0x42000000) ))))
#define D5_INPUT_BIT    (*((volatile uint8_t*) ((((((GPIO154_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(24*4)+0x42000000) ))))
#define D6_INPUT_BIT    (*((volatile uint8_t*) ((((((GPIO157_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(24*4)+0x42000000) ))))
#define D7_INPUT_BIT    (*((volatile uint8_t*) ((((((GPIO125_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(24*4)+0x42000000) ))))
// HANDSHAKE INPUTS
#define RXF_INPUT_BIT   (*((volatile uint8_t*) ((((((GPIO122_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(24*4)+0x42000000) ))))
#define TXE_INPUT_BIT   (*((volatile uint8_t*) ((((((GPIO121_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(24*4)+0x42000000) ))))
// HANDSHAKE OUTPUTS
#define RD_OUTPUT_BIT   (*((volatile uint8_t*) ((((((GPIO124_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))
#define WR_OUTPUT_BIT   (*((volatile uint8_t*) ((((((GPIO046_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))


void fifoTest(void) {
  uint8_t message[20] = {'c','i','t','i','z','e','n',' ','s','c','i','e','n','t','i','s','t','!',' '};
  for (uint8_t i = 0; i < 20; i++){
    fifoSendByte(message[i]);
  }
}


void fifoInit(void) {
  //D0 GPIO105 (UART TX)
  GPIO105_PIN_CONTROL_2_REGISTER = FIFO_PC_2;
  //D1 GPIO104 (UART RX)
  GPIO104_PIN_CONTROL_2_REGISTER = FIFO_PC_2;
  //D2 GPIO155
  GPIO155_PIN_CONTROL_2_REGISTER = FIFO_PC_2;
  //D3 GPIO010
  GPIO010_PIN_CONTROL_2_REGISTER = FIFO_PC_2;
  //D4 GPIO047
  GPIO047_PIN_CONTROL_2_REGISTER = FIFO_PC_2;
  //D5 GIO154
  GPIO154_PIN_CONTROL_2_REGISTER = FIFO_PC_2;
  //D6 GPIO157
  GPIO157_PIN_CONTROL_2_REGISTER = FIFO_PC_2;
  //D7 GPIO125
  GPIO125_PIN_CONTROL_2_REGISTER = FIFO_PC_2;

  fifoSetToOutput();

  //RXF# GPIO122
  GPIO122_PIN_CONTROL_REGISTER   = FIFO_PC_1_INPUT;
  GPIO122_PIN_CONTROL_2_REGISTER = FIFO_PC_2;
  //TXE# GPIO121
  GPIO121_PIN_CONTROL_REGISTER   = FIFO_PC_1_INPUT;
  GPIO121_PIN_CONTROL_2_REGISTER = FIFO_PC_2;
  //RD# GPIO124
  GPIO124_PIN_CONTROL_REGISTER   = FIFO_PC_1_OUTPUT;
  GPIO124_PIN_CONTROL_2_REGISTER = FIFO_PC_2;
  //WR# GPIO046
  GPIO046_PIN_CONTROL_REGISTER   = FIFO_PC_1_OUTPUT;
  GPIO046_PIN_CONTROL_2_REGISTER = FIFO_PC_2;
}


void fifoSetToOutput(void) {
  // D0 GPIO105 (UART TX)
  GPIO105_PIN_CONTROL_REGISTER   = FIFO_PC_1_OUTPUT;
  // D1 GPIO104 (UART RX)
  GPIO104_PIN_CONTROL_REGISTER   = FIFO_PC_1_OUTPUT;
  // D2 GPIO155
  GPIO155_PIN_CONTROL_REGISTER   = FIFO_PC_1_OUTPUT;
  // D3 GPIO010
  GPIO010_PIN_CONTROL_REGISTER   = FIFO_PC_1_OUTPUT;
  // D4 GPIO047 
  GPIO047_PIN_CONTROL_REGISTER   = FIFO_PC_1_OUTPUT;
  // D5 GIO154
  GPIO154_PIN_CONTROL_REGISTER   = FIFO_PC_1_OUTPUT;
  // D6 GPIO157
  GPIO157_PIN_CONTROL_REGISTER   = FIFO_PC_1_OUTPUT;
  // D7 GPIO125
  GPIO125_PIN_CONTROL_REGISTER   = FIFO_PC_1_OUTPUT;
}


void fifoSetToInput(void) {
  //D0 GPIO105 (UART TX)
  GPIO105_PIN_CONTROL_REGISTER   = FIFO_PC_1_INPUT;
  //D1 GPIO104 (UART RX)
  GPIO104_PIN_CONTROL_REGISTER   = FIFO_PC_1_INPUT;
  //D2 GPIO155
  GPIO155_PIN_CONTROL_REGISTER   = FIFO_PC_1_INPUT;
  //D3 GPIO010
  GPIO010_PIN_CONTROL_REGISTER   = FIFO_PC_1_INPUT;
  //D4 GPIO147 
  GPIO147_PIN_CONTROL_REGISTER   = FIFO_PC_1_INPUT;
  //D5 GIO154
  GPIO154_PIN_CONTROL_REGISTER   = FIFO_PC_1_INPUT;
  //D6 GPIO157
  GPIO157_PIN_CONTROL_REGISTER   = FIFO_PC_1_INPUT;
  //D7 GPIO125
  GPIO125_PIN_CONTROL_REGISTER   = FIFO_PC_1_INPUT;

}


uint8_t fifoRecvByte(void) {
  uint8_t byte = 0;
  fifoSetToInput();
  //RXF goes to 0 -> ftdi has a byte for me
  while (RXF_INPUT_BIT)
    ;
  //I pull RD down
  RD_OUTPUT_BIT = 0x00;
  //I read data from d0-d7
  if(D0_INPUT_BIT)
    byte |= 0x80;
  if(D1_INPUT_BIT)
    byte |= 0x40;
  if(D2_INPUT_BIT)
    byte |= 0x20;
  if(D3_INPUT_BIT)
    byte |= 0x10;
  if(D4_INPUT_BIT)
    byte |= 0x08;
  if(D5_INPUT_BIT)
    byte |= 0x04;
  if(D6_INPUT_BIT)
    byte |= 0x02;
  if(D7_INPUT_BIT)
    byte |= 0x01;
  //I pull RD up
  RD_OUTPUT_BIT = 0x01;
  return byte;
}


void fifoSendByte(uint8_t byte) {
  // check if TXE is 0
  fifoSetToOutput();
  while (TXE_INPUT_BIT)
    ;
  if(byte & 0x01)
    D0_OUTPUT_BIT = 0x01;
  else
    D0_OUTPUT_BIT = 0x00;
  if(byte & 0x02)
    D1_OUTPUT_BIT = 0x01;
  else
    D1_OUTPUT_BIT = 0x00;
  if(byte & 0x04)
    D2_OUTPUT_BIT = 0x01;
  else
    D2_OUTPUT_BIT = 0x00;
  if(byte & 0x08)
    D3_OUTPUT_BIT = 0x01;
  else
    D3_OUTPUT_BIT = 0x00;
  if(byte & 0x10)
    D4_OUTPUT_BIT = 0x01;
  else
    D4_OUTPUT_BIT = 0x00;
  if(byte & 0x20)
    D5_OUTPUT_BIT = 0x01;
  else
    D5_OUTPUT_BIT = 0x00;
  if(byte & 0x40)
    D6_OUTPUT_BIT = 0x01;
  else
    D6_OUTPUT_BIT = 0x00;
  if(byte & 0x80)
    D7_OUTPUT_BIT = 0x01;
  else
    D7_OUTPUT_BIT = 0x00;
  // pull WR down
  WR_OUTPUT_BIT = 0x00;
  // wait 5ns
  wait_us(1);
  // pull WR up after at least 30ns
  WR_OUTPUT_BIT = 0x01;
}




