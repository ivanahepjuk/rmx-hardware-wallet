/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2019 m2049r@monerujo.io
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __UTIL_H_
#define __UTIL_H_

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include "util.h"
#include "uart.h"
#include "crypto.h"

/** 
  For enabling debug functions, you must write 0x1 into this register, 
  also setting JTAG pullups etc is possible here
**/
#define DEBUG_ENABLE_REGISTER     (*((volatile unsigned int*) 0x4000FC20))


/** Sends ROM API version over serial port **/
void apiVersion(void);

/** Returns size of  string **/
size_t strlenX(const char* str);


/** Wasting miliseconds in a loop **/
void wait(int msec);


/* Waisting microseconds in a loop */
void wait_us(int usec);


/** Converting hexadecimal number to char 0-F **/
char hexDigit(unsigned n);


/** Conversion from char to hexadecimal representation, stores a string into "hex" **/
void charToHex(char c, char hex[3]);


/** Copying memory - embedded binary blob - from ROM no SRAM **/
void copyRomToSram(void); 


/** Converts 64bit uint into string **/
char* itoaa(uint64_t i, char b[]);


/** converts string into 32bit integer **/
int32_t aatoi(char* str);


/** Converting a string with a hexadecimal little endian representation into a array of unsigned bytes **/
const uint8_t *fromhexLE(const char *str);


/** Converting an array of unsigned bytes with a known length (inlen) into a little endian hexadecimal string **/
const char *tohexLE(const uint8_t * in, size_t inlen);


/** Converting a string with a hexadecimal big endian representation into a array of unsigned bytes **/
const uint8_t *fromhexBE(const char *str);


/** Converting an array of unsigned bytes with a known length (inlen) into a big endian hexadecimal string **/
const char *tohexBE(const uint8_t * in, size_t inlen);


/** Takes a 32B key and creates some picture out of it **/
void showColors(uint8_t key[]);


/** Wipe a memory with zeros **/
bool memzero(uint8_t * array, uint32_t length);


/** Prints integer over serial port **/
void uartNum(uint64_t num);

#endif
