/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. Shall the abovementioned 
 * disclaimer provide stricter or different rule, it is considered additional term permitted
 * by the GNU General Public License and it prevails to the extent it does not constitute
 * the further restriction according to section 10. of the GNU General Public License.
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "menus.h"
#include "util.h"
#include "display.h"
#include "buttons.h"

#include "benchmark.h"
#include "communication.h"
#include "init.h"
#include "atecc508a.h"
#include "efuse.h"
#include "xmpp.h"
#include "login.h"

#include "mec2016_rom_api.h"
#include "rng.h"
#include "flash_memory.h"
#include "storage.h"
#include "monero_sync.h"
#include "monero_storage.h"
#include "monero_messages.h"


#include "tests.h"


// #######################################################################################################################
// ########          menu EXPERIMENTAL          ##########################################################################
// #######################################################################################################################
const MENU Experimental = {11,
                          {"Device test",
                           "empty",
                           "empty",
                           "empty", 
                           "empty",
                           "empty", 
                           "empty",
                           "API version", 
                           "Check efuse", 
                           "ATECC508A",
                           "\0"
                          },
                          {
                           menuFunctionExperimental_0,
                           menuFunctionExperimental_1,
                           menuFunctionExperimental_2,
                           menuFunctionExperimental_3,
                           menuFunctionExperimental_4,
                           menuFunctionExperimental_5,
                           menuFunctionExperimental_6,
                           menuFunctionExperimental_7,
                           menuFunctionExperimental_8,
                           menuFunctionExperimental_9,
                           NULL,
                           NULL
                          }};
// Callback functions for each menu line
bool menuFunctionExperimental_0(void) {
  if(initTesting(TEST_KEY_IMAGES)) {
    errorMessage("initTesting()");
    return 1;
  }
  if(testKeyImage()) {
    errorMessage("testKeyImage()");
    return 1;
  }
  return 0;
}
bool menuFunctionExperimental_1(void) {
  placeholder();
  return 0;
}
bool menuFunctionExperimental_2(void) {
  placeholder();
  return 0;
}
bool menuFunctionExperimental_3(void) {
  placeholder();
  return 0;
}
bool menuFunctionExperimental_4(void) {
  placeholder();
  return 0;
}
bool menuFunctionExperimental_5(void) {
  placeholder();
  return 0;
}
bool menuFunctionExperimental_6(void) {
  placeholder();
  return 0;
}
bool menuFunctionExperimental_7(void) {
  apiVersion();
  return 0;
}
bool menuFunctionExperimental_8(void) {
  checkEfuse();
  return 0;
}
bool menuFunctionExperimental_9(void) {
  atecDumpConfig();
  return 0;
}


// #######################################################################################################################
// ########          menu RNG          ###################################################################################
// #######################################################################################################################
const MENU Rng        = {4,
                        {"CEC1702 RNG chars",
                         "CEC1702 RNG hexa",
                         "External",
                         "\0",
                         "\0",
                         "\0",
                         "\0",
                         "\0",
                         "\0",
                         "\0",
                         "\0"
                        },{
                         menuFunctionRng_0,
                         menuFunctionRng_1,
                         menuFunctionRng_2,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL
                        }};
// Callback functions for each menu line
bool menuFunctionRng_0(void) {
  rngCEC1702chars();
  return 0;
}
bool menuFunctionRng_1(void) {
  rngCEC1702hexa();
  return 0;
}
bool menuFunctionRng_2(void) {
  rngExternalInit();
  return 0;
}


// #######################################################################################################################
// ########          menu MONERO          ################################################################################
// #######################################################################################################################

const MENU Monero     = {5, 
                        {"View balance",
                         "Receive",
                         "Send",
                         "Setup",
                         "\0",
                         "\0",
                         "\0",
                         "\0",
                         "\0",
                         "\0",
                         "\0"
                        },{
                         menuFunctionMonero_0,
                         menuFunctionMonero_1,
                         menuFunctionMonero_2,
                         menuFunctionMonero_3,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL
                        }};
// Callback functions for each menu line
bool menuFunctionMonero_0(void) {
  login();
  moneroSynchronize();

  if(finalize1()) {
    uxMessage(" Finalizing 1", 50);
  }

  if(finalize2()) {
    uxMessage(" Finalizing 2", 50);
  }

  debounce();
  return 0;
}
bool menuFunctionMonero_1(void) {
  shareMoneroAddress();
  return 0;
}
bool menuFunctionMonero_2(void) {
  placeholder();
  return 0;
}
bool menuFunctionMonero_3(void) {
  debounce();
  showMenu(&Monero_setup);
  return 0;
}


// #######################################################################################################################
// ########          menu MONERO SETUP    ################################################################################
// #######################################################################################################################

const MENU Monero_setup = {6, 
                        {"Node address",
                         "Set blockheight",
                         "DumpTXs",
                         "finalize1",
                         "finalize2",
                         "\0",
                         "\0",
                         "\0",
                         "\0",
                         "\0",
                         "\0"
                        },{
                         menuFunctionMonero_setup_0,
                         menuFunctionMonero_setup_1,
                         menuFunctionMonero_setup_2,
                         menuFunctionMonero_setup_3,
                         menuFunctionMonero_setup_4,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL
                        }};
// Callback functions for each menu line
bool menuFunctionMonero_setup_0(void) {
  if(setupNode())
    errorMessage("err: setup node");    
  return 0;
}
bool menuFunctionMonero_setup_1(void) {
  if(setupBlockheight())
    errorMessage("err: setup blckhght");
  return 0;
}
bool menuFunctionMonero_setup_2(void) {
  dumpTxStorage();
  return 0;
}
bool menuFunctionMonero_setup_3(void) {
  finalize1();
  debounce();
  return 0;
}
bool menuFunctionMonero_setup_4(void) {
  finalize2();
  debounce();
  return 0;
}





// #######################################################################################################################
// ########          menu SETUP          #################################################################################
// #######################################################################################################################
const MENU Setup      = {6,  
                         {"Dump EFUSE OTP",
                          "Dump ATECC508A",
                          "empty",
                          "Show seed colours",
                          "Storage init",
                          "\0",
                          "\0",
                          "\0",
                          "\0",
                          "\0",
                          "\0"
                         },{
                          menuFunctionMenu_0,
                          menuFunctionMenu_1,
                          menuFunctionMenu_2,//newSeedInit,//firstInit,
                          menuFunctionMenu_3,//showColors,
                          menuFunctionMenu_4,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          NULL
                         }};
// Callback functions for each menu line
bool menuFunctionMenu_0(void) {
  checkEfuse();
  return 0;
}
bool menuFunctionMenu_1(void) {
  atecDumpConfig();
  return 0;
}
bool menuFunctionMenu_2(void) {
  placeholder();
  return 0;
}
bool menuFunctionMenu_3(void) {
  placeholder();
  return 0;
}
bool menuFunctionMenu_4(void) {
  initPersistentStorage();
  return 0;
}

// #######################################################################################################################
// ########          menu XMPP          #############################################################################
// #######################################################################################################################
const MENU Xmpp  = {8,  
                   {"XMPP Login",
                    "Write message",
                    "Read message",
                    "Add contact",
                    "Delete contact",
                    "Setup my login",
                    "Browse contacts",
                    "\0",
                    "\0",
                    "\0",
                    "\0"
                   },{
                    menuFunctionXmpp_0,
                    menuFunctionXmpp_1,
                    menuFunctionXmpp_2,
                    menuFunctionXmpp_3,
                    menuFunctionXmpp_4,
                    menuFunctionXmpp_5,
                    menuFunctionXmpp_6, 
                    NULL,
                    NULL,
                    NULL,
                    NULL
                   }};
// Callback functions for each menu line
bool menuFunctionXmpp_0(void) {
  //XMPPLogin();
  return 0;
}
bool menuFunctionXmpp_1(void) {
  //XMPPSendMessage();
  return 0;
}
bool menuFunctionXmpp_2(void) {
  //XMPPReadMessagesMenu();
  return 0;
}
bool menuFunctionXmpp_3(void) {
  //XMPPAddContact();
  return 0;
}
bool menuFunctionXmpp_4(void) {
  //deleteContact();
  return 0;
}
bool menuFunctionXmpp_5(void) {
  //XMPPLoginSetup();
  return 0;
}
bool menuFunctionXmpp_6(void) {
  //browseContacts();
  return 0;
}


