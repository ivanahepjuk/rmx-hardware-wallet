/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OLEDTWO_H
#define OLEDTWO_H

#include <string.h>
#include <stdlib.h>
#include "stdint.h"
#include "stdbool.h"
#include "spi.h"
#include "logos_bitmaps.h"
#include "util.h"
#include "fonts.h"

/**************************************************************************************/
/*       Code used by 128x128 RGB OLED display, ssd1351 driver:                       */
/**************************************************************************************/

#define OLED_BUFSIZE (128*128*2)
#define OLED_WIDTH   128
#define OLED_HEIGHT  128

#define SSD1351_SET_COLUMN       	 0x15
#define SSD1351_SET_ROW          	 0x75
#define SSD1351_WRITERAM        	 0x5C
#define SSD1351_READRAM         	 0x5D
#define SSD1351_SET_REMAP        	 0xA0
#define SSD1351_SET_STARTLINE      0xA1
#define SSD1351_SET_OFFSET   	     0xA2
#define SSD1351_MODE_ALLOFF   	   0xA4
#define SSD1351_MODE_ALLON    	   0xA5
#define SSD1351_MODE_NORMAL   	   0xA6
#define SSD1351_MODE_INVERSE  	   0xA7
#define SSD1351_FUNCTIONSELECT  	 0xAB
#define SSD1351_SLEEP_ON         	 0xAE
#define SSD1351_SLEEP_OFF       	 0xAF
#define SSD1351_SET_RESETPRECHARGE 0xB1
#define SSD1351_SET_ENHANCEMENT    0xB2
#define SSD1351_SET_CLOCK          0xB3
#define SSD1351_SET_VSL          	 0xB4
#define SSD1351_SET_GPIO         	 0xB5
#define SSD1351_SET_PRECHARGE2     0xB6
#define SSD1351_SET_GRAYTABLE      0xB8
#define SSD1351_USELUT          	 0xB9
#define SSD1351_SET_PRECHARGEVOLT	 0xBB
#define SSD1351_SET_VCOMH     	   0xBE
#define SSD1351_SET_CONTRASTABC    0xC1
#define SSD1351_SET_CONTRASTMASTER 0xC7
#define SSD1351_SET_MUXRATIO       0xCA
#define SSD1351_SET_COMMANDLOCK    0xFD
#define SSD1351_SET_SCROLL         0x96
#define SSD1351_STOPSCROLL         0x9E
#define SSD1351_STARTSCROLL        0x9F



void oledInit(void);
void oledReset(void);
void fillBuf(uint16_t fillcolor);
void writeBuf(void);




void drawPixel(uint8_t x, uint8_t y, uint16_t color);
void drawLine(int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color);
void fillRect(int16_t x, int16_t y, uint16_t w, uint16_t h, uint16_t color);
void fillCircle(int16_t x0, int16_t y0, uint16_t radius, uint16_t color);


#define CR  0x0D 
#define LF  0x0A
void writeText(uint8_t  x, uint8_t y, char text[], uint16_t color);















void placePictureWithAnimtext(uint8_t img_x, uint8_t img_y, const BITMASK *img, int text_y, const char* text, uint16_t color, uint8_t zoom);
void animText(int x, int y, const char* text, uint16_t color, uint8_t zoom);


void placeImage( uint8_t x, uint8_t y, const BITMASK *img);
void drawMessage(char* text, uint16_t color, uint8_t zoom);

void drawStringCenteredZoom(int y, const char* text, uint16_t color, uint8_t zoom);
void drawStringZoom(int x, int y, const char* text, uint16_t color, uint8_t zoom);
void drawChar(char c, int16_t x, int16_t y, uint16_t color, uint8_t zoom);
//void placeImage2( uint8_t x, uint8_t y, const BITMAP *img);






void openRect(int16_t x, int16_t y, uint16_t w, uint16_t h, uint16_t color);

void openCircle(int16_t x0, int16_t y0, uint16_t radius, uint16_t color);





#define LEFTRIGHT    0
#define YESNO        1
#define OK           2
void placeNavigation(uint8_t type);



void progressBarCircle(int8_t progress, uint16_t color, bool dir);
void progressBarCircle2(int8_t progress, uint16_t color, bool dir);





#endif
