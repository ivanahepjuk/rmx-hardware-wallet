/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 872E 748B 97F7 8E39 8D61  706D 2A81 682B 4BE0 C6A5> <i_a@rmxwallet.org>
 * Copyright (C) m2049r <m2049r@monerujo.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *   - This program also includes the software created under the copyright of Aleksey Kravchenko 
 *     <rhash.admin@gmail.com> (2013) et al.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * This program includes sha3.h - an implementation of Secure Hash Algorithm 3 (Keccak)
 * based on The Keccak SHA-3 submission. Submission to NIST (Round 3), 2011 by Guido Bertoni, Joan Daemen, Michaël Peeters and Gilles Van      
 * Assche
 */

#ifndef SHA3_H
#define SHA3_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#define KEYSIZE 32
#define AMOUNTSIZE 8

/*
typedef uint8_t key32_t[KEYSIZE];

#define SCALARSIZE 32
typedef uint8_t scalar32_t[SCALARSIZE];
typedef uint8_t scalar64_t[2*SCALARSIZE];

typedef union {
  scalar64_t xy;
  struct {
    scalar32_t x, y;
  };
} coord32_t;

typedef struct {
  key32_t R;
  size_t size;
  key32_t* P;
} output_list_t;


typedef struct {
  key32_t key_C;
  key32_t key_D;
} subaddress64_t;
*/


/*
//crypto
extern coord32_t const G;

#define NO_COMPRESSION    0
#define COMPRESS          1
void scalarMult(key32_t aP, const scalar32_t a, const coord32_t *P, bool compress);
void scalarMultKey(key32_t aP, const scalar32_t a, const key32_t P);
void scalarMultBase(key32_t aG, const scalar32_t a);

void reduce32(key32_t x);
void div_modp(scalar32_t r, const scalar32_t a, const scalar32_t b);
void modp(scalar32_t x);
void subtract_modp(scalar32_t r, const scalar32_t a, const scalar32_t b);
void add_modp(scalar32_t r, const scalar32_t a, const scalar32_t b);
void div_modp(scalar32_t r, const scalar32_t a, const scalar32_t b);
void mult8_modp(scalar32_t r, const scalar32_t a);
void minus_modp(scalar32_t x); //experiment
void derivation_to_scalar(const key32_t key, const size_t output_index, scalar32_t res);
void recoverPoint(coord32_t *Point, const key32_t P);
bool calcBprime(key32_t B, const scalar32_t h, const coord32_t *A, const coord32_t *R);

void keyImage(const key32_t x, key32_t P, key32_t I);
void genCommitmentMask(const key32_t sk, key32_t result);
void generate_key_image(const key32_t public_key, const key32_t secret_key, key32_t key_image);

void ecdhHash(const scalar32_t k, scalar32_t hash);
void xor8(const scalar32_t hash, uint8_t amount[]);
void amountToMoney(const uint8_t decrypted_amount[], char money[]);
*/


//sha3
bool keccak_256(const unsigned char* data, size_t len, unsigned char* digest);


















#endif
