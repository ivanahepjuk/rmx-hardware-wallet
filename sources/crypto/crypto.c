/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 872E 748B 97F7 8E39 8D61  706D 2A81 682B 4BE0 C6A5> <i_a@rmxwallet.org>
 * Copyright (C) 2018m2049r <m2049r@monerujo.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *   - This program also includes the software created under the copyright of Aleksey Kravchenko 
 *     <rhash.admin@gmail.com> (2013) et al.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * This program includes sha3.h - an implementation of Secure Hash Algorithm 3 (Keccak)
 * based on The Keccak SHA-3 submission. Submission to NIST (Round 3), 2011 by Guido Bertoni, Joan Daemen, Michaël Peeters and Gilles Van      
 * Assche
 */

#include "crypto.h"
#include "mec2016_rom_api.h"
#include <sys/types.h>
#include "util.h"
#include "uart.h"
#include "sha3.h"

#include <string.h>

#include "mnemonics.h"
#include "memory.h"

#include "key_images.h"
// keccak
#define sha3_max_permutation_size 25
#define sha3_max_rate_in_qwords 24
#define keccak_256_Init sha3_256_Init
#define keccak_Update sha3_Update
#define SHA3_FINALIZED 0x80000000
#define IS_ALIGNED_64(p) (0 == (7 & ((const char*)(p) - (const char*)0)))
#define ROTL64(qword, n) ((qword) << (n) ^ ((qword) >> (64 - (n))))
#define I64(x) x##LL
#define MEMSET_BZERO(p,l) memset(((uint8_t*) p), 0, (l))
#define le2me_64(x) (x)
#define me64_to_le_str(to, from, length) memcpy((to), (from), (length))

/* constants */
#define NumberOfRounds 24



// begin Ed25519 stuff
/*
 * Prime
 * p = (2**255 - 19)
 *   = 0x7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffed
 */
// LSBF
const scalar32_t p25519 = {0xed, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
                            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
                            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
                            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x7f};

/*
 * Order
 * l = (2**252 + 27742317777372353535851937790883648493)
 *   = 0x1000000000000000000000000000000014def9dea2f79cd65812631a5cf5d3ed
 */
// LSBF
const scalar32_t l25519 = {0xed, 0xd3, 0xf5, 0x5c, 0x1a, 0x63, 0x12, 0x58,
                            0xd6, 0x9c, 0xf7, 0xa2, 0xde, 0xf9, 0xde, 0x14,
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10};

/*
 * H = h * G
 * where a = a[0]+256*a[1]+...+256^31 a[31]
 * G is the Ed25519 base point (x,4/5) with x positive.
 *
 * Preconditions:
 *  a[31] <= 127
 *
 * Ed25519 Base Point in LSBF
 */
const coord32_t G = { .x = {0x1a, 0xd5, 0x25, 0x8f, 0x60, 0x2d, 0x56, 0xc9, 0xb2, 0xa7, 0x25, 0x95, 0x60, 0xc7, 0x2C, 0x69, 0x5c, 0xdc, 0xd6, 0xfd, 0x31, 0xe2, 0xa4, 0xc0, 0xfe, 0x53, 0x6e, 0xcd, 0xd3, 0x36, 0x69, 0x21},
                      .y = {0x58, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66}};

// some constants
const uint8_t ONE[1] = {0x01};
const uint8_t ZERO[1] = {0x00};
const uint8_t EIGHT[1] = {0x08};
const uint8_t TWO[1] = {0x02};

#define SCALARMULT_FLAGS 0b00 // e=LSBF, px/py=LSBF//fixme 0x00 better?



void reduce32(scalar32_t x) {
  // 0x04 C = B mod P (P odd), A is ignored
  // bit.7 = 0: Field F(p)
  // 0x04nn: 256 bit operand
  // bit.16 = 0: LSBF
  api_pke_modular_math(0x00000404, l25519, 32, NULL, 0, x, KEYSIZE);
  api_pke_start(false);
  while (api_pke_busy());
  api_pke_copy_from_scm(x, KEYSIZE, 0x3, false);
}

void modp(scalar32_t x) {
  // 0x04 C = B mod P (P odd), A is ignored
  // bit.7 = 0: Field F(p)
  // 0x04nn: 256 bit operand
  // bit.16 = 0: LSBF
  api_pke_modular_math(0x00000404, p25519, 32, NULL, 0, x, KEYSIZE);
  api_pke_start(false);
  while (api_pke_busy());
  api_pke_copy_from_scm(x, KEYSIZE, 0x3, false);
}




void subtract_modp(scalar32_t r, const scalar32_t a, const scalar32_t b) {
  // 0x02 C = (A-B) mod P
  api_pke_modular_math(0x00000402,
    p25519, 32,     // P
    a, SCALARSIZE,  // A
    b, SCALARSIZE); // B
  api_pke_start(false);
  while (api_pke_busy());
  api_pke_copy_from_scm(r, SCALARSIZE, 0x3, false);
}

void add_modp(scalar32_t r, const scalar32_t a, const scalar32_t b) {
  // 0x01 C = (A+B) mod P
  api_pke_modular_math(0x00000401,
    p25519, 32,     // P
    a, SCALARSIZE,  // A
    b, SCALARSIZE); // B
  api_pke_start(false);
  while (api_pke_busy());
  api_pke_copy_from_scm(r, SCALARSIZE, 0x3, false);
}

void div_modp(scalar32_t r, const scalar32_t a, const scalar32_t b) {
  // 0x05 C = (A/B) mod P
  api_pke_modular_math(0x00010405,
    p25519, 32,     // P
    a, SCALARSIZE,  // A
    b, SCALARSIZE); // B
  api_pke_start(false);
  while (api_pke_busy());
  api_pke_copy_from_scm(r, SCALARSIZE, 0x3, false);
}

void mult8_modp(scalar32_t r, const scalar32_t a) {
  // 0x03 C = (A*B) mod P (P odd)
  api_pke_modular_math(0x00000403,
    p25519, 32,     // P
    EIGHT, 1,       // A
    a, SCALARSIZE); // B
  api_pke_start(false);
  while (api_pke_busy());
  api_pke_copy_from_scm(r, SCALARSIZE, 0x3, false);
}

//experiment
void minus_modp(scalar32_t x) {
  // 0x03 C = (A*B) mod P (P odd)
  // bit.7 = 0: Field F(p)
  // 0x04nn: 256 bit operand
  // bit.16 = 0: LSBF
  scalar32_t twice_x;
  api_pke_modular_math(0x00000403, p25519, 32, TWO, 1, x, KEYSIZE);
  api_pke_start(false);
  while (api_pke_busy());
  api_pke_copy_from_scm(twice_x, KEYSIZE, 0x3, false);
  //now subtract
  scalar32_t save_x;
  memcpy(save_x, x, SCALARSIZE);
  subtract_modp(x, save_x, twice_x);
}





void recoverPoint(coord32_t *Point, const key32_t P) {
  // P is y-coordinate with P[31]&&0x80 if x[0]&&0x01
  memcpy(Point->y, P, SCALARSIZE);

  // Extract the parity bit
  uint8_t parity = (Point->y[31] >> 7);

  // Zero the parity bit
  Point->y[31] &= 0b01111111;

  // recover X
  api_pke_ed25519_xrecov(Point->y, KEYSIZE, false);
  api_pke_start(false);
  while (api_pke_busy());
  api_pke_copy_from_scm(Point->x, KEYSIZE, 0x6, false);


  // determine the "sign" of the final x coord based on the parity bit in the y
  // see zero to monero p.18
  if (parity != (Point->x[0] & 0x01)){
    minus_modp(Point->x);
  }
}

void scalarMult(key32_t aP, const scalar32_t a, const coord32_t *P) {//, bool compress) {
  // multiply
  api_pke_ed25519_scalarmult(P->x, 32, P->y, 32, a, 32, SCALARMULT_FLAGS);
  api_pke_start(false);
  while (api_pke_busy());
  scalar32_t x;
  api_pke_copy_from_scm(x, KEYSIZE, 0xA, false);
  api_pke_copy_from_scm(aP, KEYSIZE, 0xB, false);
  //commented out because caused errors in xmpp payload
  aP[31] &= ~0x80;
///  if(COMPRESS) {
    // compress - tested and correct!
    if ((x[0] & 0x01))  //FIXME maybe bug, 0x80 and not 0x01?
      aP[31] |= 0x80;
    else // not sure this is necessary
      aP[31] &= ~0x80;
///  }
}

//multiply with scalar (scalar32_t a) and some ed25519 point (key32_t P)
void scalarMultKey(key32_t aP, const scalar32_t a, const key32_t P) {
  coord32_t Point;
  recoverPoint(&Point, P);
  scalarMult(aP, a, &Point);//, COMPRESS);
}

void scalarMultBase(key32_t aG, const scalar32_t a) {
  scalarMult(aG, a, &G);//, COMPRESS);
}




void hash_to_scalar(const void *data, size_t length, scalar32_t res) {
  keccak_256(data, length, res);
  reduce32(res);
}


void hash_to_ec(const key32_t public_key, key32_t ec_point) {
  /* source: https://github.com/monero-project/monero/blob/2846d0850d6e92d38c44eac9e6a4fca6b1545453/src/crypto/crypto.cpp#L245
  static void hash_to_ec(const public_key &key, ge_p3 &res) {
    hash h;
    ge_p2 point;
    ge_p1p1 point2;
    cn_fast_hash(std::addressof(key), sizeof(public_key), h);
    ge_fromfe_frombytes_vartime(&point, reinterpret_cast<const unsigned char *>(&h));
    ge_mul8(&point2, &point);
    ge_p1p1_to_p3(&res, &point2);
  }
  */

  scalar32_t hash;
  keccak_256(public_key, 32, hash);
  // compiled math
  ge25519 point1;
  ge25519_fromfe_frombytes_vartime(&point1, hash);

  ge25519 point2;
  ge25519_mul8(&point2, &point1);

  ge25519_pack(ec_point, &point2);

}

void generate_key_image(const key32_t public_key, const key32_t secret_key, key32_t key_image) {
  /* source: https://github.com/monero-project/monero/blob/2846d0850d6e92d38c44eac9e6a4fca6b1545453/src/crypto/crypto.cpp#L255
  void crypto_ops::generate_key_image(const public_key &pub, const secret_key &sec, key_image &image) {
    ge_p3 point;
    ge_p2 point2;
    assert(sc_check(&sec) == 0);
    hash_to_ec(pub, point);
    ge_scalarmult(&point2, &sec, &point);
    ge_tobytes(&image, &point2);
  }
  */
  coord32_t Point;
  recoverPoint(&Point, public_key);
  scalarMult(key_image, secret_key, &Point);//, NO_COMPRESSION);
}





/*
  To decrypt the amount, calculate:
  amount = 8 byte encrypted amount XOR first 8 bytes of keccak("amount" || Hs(8aR||i))

  also:
  mask = x + H(K); //where x is the blinding scalar
  amount = b + H(H(K)); //where b is the amount
  `K` is the per-output Diffie-Hellman derived shared secret defined as `K=Hs(8rA||i)`
  `H` is a hash function that produces a scalar 
*/
 
  

/*

  //generate comitment mask:
  //Hs_D is transaction shared secret
  //key32_t mask;
  //genCommitmentMask(Hs_D, mask);
//void hash_to_scalar(const void *data, size_t length, scalar32_t res) {
void genCommitmentMask(const key32_t sk, key32_t result) {
  char data[15 + 32];
  memcpy(data, "commitment_mask", 15);
  memcpy(data + 15, &sk, 32);
  //key32_t scalar;
  hash_to_scalar(data, sizeof(data), result);
*/



void ecdhHash(const scalar32_t k, scalar32_t hash) {
  uint8_t data[38];
  //scalar32_t hash;
  memcpy(data, "amount", 6);
  memcpy(data + 6, k, 32);
  keccak_256(data, 38, hash);
}


void xor8(const scalar32_t hash, uint8_t amount[8]){
  for (int i = 0; i < 8; ++i)
    amount[i] ^= hash[i];  
}


void amountToMoney(const uint8_t decrypted_amount[8], char money[33]) { //fixme sizeof string?
  uint64_t amount;
  memcpy((uint8_t*)&amount, decrypted_amount, 8);
  char string[33];
  itoaa(amount, string);
  //shifting and making a space for '.'
  uint8_t str_len = strlen(string);
  for(uint8_t i = str_len+1; i>str_len - 12 - 1; i--)
    string[i+1] = string[i];
  // place a decimal dot
  string[str_len-12] = '.';
  memcpy(money, string, 32);  
}


void write_varint(char **dest, size_t i) {
  /* Make sure that there is one after this */
  while (i >= 0x80) {
    **dest = ((char) i & 0x7f) | 0x80;
    ++(*dest);
    /* It should be in multiples of 7, this should just get the next part */
    i >>= 7;			
  }
  /* writes the last one to dest */
  **dest = (char) i;
  ++(*dest);
}


void derivation_to_scalar(const key32_t key, const size_t output_index, scalar32_t res) {
  struct {
    key32_t derivation;
    char output_index[(sizeof(size_t) * 8 + 6) / 7];
  } buf;
  char *end = buf.output_index;
  memcpy(buf.derivation, key, KEYSIZE);
  write_varint(&end, output_index);

  // TODO: make this handle error properly... FIXME
  if (end > buf.output_index + sizeof buf.output_index) for(;;);

  hash_to_scalar(&buf, end - (char*)(&buf), res);
}


bool calcBprime(key32_t B, const scalar32_t h, const coord32_t *A, const coord32_t *R) {
  Ed25519_SIG_VERIFY psv;
  psv.params[ED_PARAM_AX] = A->x;
  psv.paramlen[ED_PARAM_AX] = SCALARSIZE;
  psv.params[ED_PARAM_AY] = A->y;
  psv.paramlen[ED_PARAM_AY] = SCALARSIZE;
  psv.params[ED_PARAM_RX] = R->x;
  psv.paramlen[ED_PARAM_RX] = SCALARSIZE;
  psv.params[ED_PARAM_RY] = R->y;
  psv.paramlen[ED_PARAM_RY] = SCALARSIZE;
  psv.params[ED_PARAM_SIG] = ZERO;
  psv.paramlen[ED_PARAM_SIG] = 1;
  psv.params[ED_PARAM_HASH] = h;
  psv.paramlen[ED_PARAM_HASH] = SCALARSIZE;
  psv.flags = 0; // all params are LSBF
  psv.rsvd = 0;

  uint8_t ok = api_pke_ed25519_valid_sig(&psv);
  if (ok != 0) {
//    uartSend("\r\napi_pke_ed25519_valid_sig returns "); uartSendChar_to_hex(ok);
    return false;
  }
  api_pke_start(false);
  while (api_pke_busy());
//  const uint32_t status = api_pke_status();
//  uartSend("\r\n done: "); uartSend(tohexLE((uint8_t*) &status, 4));
/*  coord32_t P1, P2, P3;
  api_pke_copy_from_scm(P1.x, SCALARSIZE, 0xA, false);
  api_pke_copy_from_scm(P1.y, SCALARSIZE, 0xB, false);
  api_pke_copy_from_scm(P2.x, SCALARSIZE, 0xC, false);
  api_pke_copy_from_scm(P2.y, SCALARSIZE, 0xD, false);
  api_pke_copy_from_scm(P3.x, SCALARSIZE, 0xE, false);
  api_pke_copy_from_scm(P3.y, SCALARSIZE, 0xF, false);*/
  api_pke_copy_from_scm(B, SCALARSIZE, 0xF, false);
/*  uartSend("\r\nP1x: "); uartSend(tohexLE(P1.x, SCALARSIZE));
  uartSend("\r\nP1y: "); uartSend(tohexLE(P1.y, SCALARSIZE));
  uartSend("\r\nP2x: "); uartSend(tohexLE(P2.x, SCALARSIZE));
  uartSend("\r\nP2y: "); uartSend(tohexLE(P2.y, SCALARSIZE));
  uartSend("\r\nP3x: "); uartSend(tohexLE(P3.x, SCALARSIZE));
  uartSend("\r\nP3y: "); uartSend(tohexLE(P3.y, SCALARSIZE));*/
  return true;
}







      



