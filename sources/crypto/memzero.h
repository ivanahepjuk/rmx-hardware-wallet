#ifndef __MEMZERO_H__
#define __MEMZERO_H__

#include <stddef.h>

void memmzero(void * const pnt, const size_t len);

#endif
