/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

 
#include "login.h"
#include "util.h"
#include "crypto.h"
#include "buttons.h"
#include "menus.h"
#include "efuse.h"
#include "sha3.h"

#include "mnemonics.h"
#include "flash_memory.h"
#include "storage.h"


/**
  This login function will be replaced with a secure element exchange. 
**/
bool login(void) {
  /*
  * account switch:
  * octopussy 0
  * thehood   1
  * user3     2
  * user4     3
  * user5     4
  */
  int8_t account = 2; /* switch here */
  switch (account) {
    case 0: 
      memcpy(privkey, fromhexLE("5a877db95d9c89d296499cf0d3054e15b12b5502dea7be95b6d1cc04493cc80f"), KEYSIZE);  //wallet1 gitlab docs
      break;
    case 1: 
      memcpy(privkey, fromhexLE("6bcb164908642918b660096529f92c40b22a33ae3d0bf73c594c0b21039eba00"), KEYSIZE);  
      break;
    case 2: 
      memcpy(privkey, fromhexLE("d55f93b7d97667adc507a761fc6f13c242b0e2e1da3a4aec3f16d242fb036c0d"), KEYSIZE);  //wallet3 gitlab docs
      break;
    case 3: 
      memcpy(privkey, fromhexLE("6bcb164908642918b660096529f92c40b22a33ae3d0bf73c594c0b21039eba00"), KEYSIZE);
      break;
    case 4: 
      memcpy(privkey, fromhexLE("5a877db95d9c89d296499cf0d3054e15b12b5502dea7be95b6d1cc04493cc84f"), KEYSIZE);
      break;
  }

  // storage inits here
  memzero((uint8_t*)monero_data, MONERO_STORAGE_SIZE);
  memzero((uint8_t*)xmpp_data, XMPP_STORAGE_SIZE);
  memzero(aes_output,      AES_BLOCK_SIZE);
  memzero(aes_input,       AES_BLOCK_SIZE);
  memzero(aes_key_array,   KEYSIZE);
  memzero(aes_init_vector, KEYSIZE);
  //memzero(privkey, KEYSIZE);

  deriveAesIv(privkey, aes_init_vector);

  //xmpp
  spiEnable();
  globalUnlockFlash();
  resetFlash();
  readData(aes_input, XMPP_MEMORY_ADDRESS);
  spiDisable();
  cryptBlock(DECRYPT_BLOCK);
  memcpy(xmpp_data, aes_output, AES_BLOCK_SIZE);

  //monero
  uint8_t *p_monero_data = (uint8_t*)&monero_data;
  spiEnable();

  globalUnlockFlash();
  resetFlash();
  //block1
  readData(aes_input, MONERO_MEMORY_ADDRESS);
  cryptBlock(DECRYPT_BLOCK);
  memcpy(&p_monero_data[0], &aes_output, AES_BLOCK_SIZE);
  //block2
  readData(aes_input, MONERO_MEMORY_ADDRESS + 4096);
  cryptBlock(DECRYPT_BLOCK);
  memcpy(&p_monero_data[4096], &aes_output, AES_BLOCK_SIZE);
  spiDisable();

  return 0;
}


  
void deriveKey(uint8_t *password){
  //fixme add length checking
  keccak_256((uint8_t*)password, 32, password);
  for(uint8_t rounds = 0; rounds<100; rounds++) {
    keccak_256(password, KEYSIZE, password);
  }
}



bool deriveAesIv (const uint8_t *priv_key, uint8_t *iv) {   
  keccak_256(priv_key, 32, iv);
  for(uint8_t i = 0; i<100; i++) {
    keccak_256(iv, 32, iv);
  }
  return true;//checkme fixme check?
}


/*
void newSeedInit(uint8_t slot_num) {

  uartSend("Started initialization of seed slot n.");
  uartSendChar_to_hex(slot_num+1);
  uartSend("\r\n");

  fillBuf(0x0000);
  placeNavigation(OK);
  animText(0, 0, "Seed init,", 0xffff, 1);
  animText(0, 10, "Press OK to", 0xffff, 1);
  animText(0, 20, "start initialization.", 0xffff, 1);
  {
  waitForOK();  fillBuf(0x0000);
  placeNavigation(LEFTRIGHT);
  animText(5, 35, "Carefully backup words", 0xffff, 1);
  animText(5, 45, "with paper and pencil", 0xffff, 1);
  animText(5, 55, "and store it somewhere.", 0xffff, 1);
  animText(5, 65, "They are not going to", 0xffff, 1);
  animText(5, 75, "appear again!", 0xffff, 1);
  wait(1000);
  animText(5, 95, "Press OK to continue", 0xffff, 1);
  waitForOK();
  }

  SECRET Seed;
  SECRET Passphrase;

  SecretInitToZeroes(&Seed);
  SecretInitToZeroes(&Passphrase);


  // GENERATING SEED HERE 
  //create random 
  generateRandom(Seed.entropy);
  Seed.entropy_v = KEYSIZE;

  uartSend("Seed random generated: ");
  uartSend(tohexLE(Seed.entropy, KEYSIZE)); 

  bytesToIndices(&Seed, IS_SEED);
  
  //show wordlist
  uartSend("\r\n Words : ["); 
  for(uint8_t i = 0; i<Seed.indices_v; i++) {
  uartSend(mnemonicWord(Seed.indices[i]));
  if(i<24)
    uartSend(", ");
  }
  uartSend("]\r\n");

  // Paper backup here 
  int8_t wordposition = 0;
  bool update = true;
  bool setupDone = false;

  while (!setupDone) {
    switch(scanAnyKey()) {
      case 1:
        wordposition--;
        update = true;
        debounce();
        break;
      case 2:
        if(wordposition == Seed.indices_v)
          setupDone = true;
        debounce();
        break;
      case 3:
        wordposition++;
        update = true;
        debounce();
        break;
      default:
        break;
    }
    if(wordposition < 0)
      wordposition = Seed.indices_v;
    if(wordposition > Seed.indices_v)
      wordposition = 0	;
    if(update) {
      if(wordposition >= 0 && wordposition < Seed.indices_v) {
        char number[9];
        itoaa((uint32_t)wordposition+1, number);
        fillBuf(0x0000);
        placeNavigation(LEFTRIGHT);
        //number of word
        animText(10, 10, "word #", 0xffff, 2);
        animText(80, 10, number, 0xffff, 2);
        animText(5, 70, mnemonicWord(Seed.indices[wordposition]), 0xffff, 2);
        update = false;
      }
      else if(wordposition == Seed.indices_v) {
        fillBuf(0x0000);
        placeNavigation(LEFTRIGHT);
        //number of word
        animText(10, 10, "Next?", 0xffff, 2);
        update = false;
      }
    }
  }

  // GENERATING ENTROPY PASSPHRASE HERE 
  { //UI block 
  fillBuf(0x0000);
  placeNavigation(LEFTRIGHT);
  animText(0, 0, "Choose size of", 0xffff, 1);
  animText(0, 10, "passphrase entropy:", 0xffff, 1);
  }

  int8_t entropy_multiplier = 1;
  update = true;
  setupDone = false;

  while (!setupDone) {
    switch(scanAnyKey()) {
      case 1:
        entropy_multiplier--;
        update = true;
        debounce();
        break;
      case 2:
        Passphrase.entropy_v = 4 * entropy_multiplier;
        setupDone = true;
        debounce();
        break;
      case 3:
        entropy_multiplier++;
        update = true;
        debounce();
        break;
      default:
        break;
    }
    if(entropy_multiplier < 1)
      entropy_multiplier = 8;
    if(entropy_multiplier > 8)
      entropy_multiplier = 1;
    if(update) {
      if(entropy_multiplier >= 1 && entropy_multiplier <=8) {
        char number[9];
        itoaa((uint32_t)entropy_multiplier*32, number);
        fillBuf(0x0000);
        placeNavigation(LEFTRIGHT);
        drawStringZoom(0, 0, "Choose size of", 0xffff, 1); 
        drawStringZoom(0, 10, "passphrase entropy:", 0xffff, 1); 
        animText(37, 37, number, 0xffff, 3);
        animText(39, 73, "[bits]", 0xffff, 2);
        update = false;
      }
    }
  }

  generateRandom(Passphrase.entropy);
//  for(uint8_t i = 0; i<32; i++)
//    Passphrase.entropy[i] = testdata[i];
  
  uartSend("Entropy for passphrase generated: ");
  uartSend(tohexLE(Passphrase.entropy, KEYSIZE)); 
  //uartSend("\r\n");


  //update entropy size
  for(uint8_t i = 0; i<KEYSIZE; i++) {
    if(i >= Passphrase.entropy_v)
      Passphrase.entropy[i] = 0;
  }

  uartSend("\r\nPassphrase entropy based on bitsize: ");
  uartSend(tohexLE(Passphrase.entropy, KEYSIZE)); 
//  uartSend("\r\n");

  bytesToIndices(&Passphrase, IS_NOT_SEED);

  //show wordlist
  uartSend("\r\nEntropy passphrase words: "); 
  for(uint8_t i = 0; i<Passphrase.indices_v; i++) {
  uartSend(mnemonicWord(Passphrase.indices[i]));
  uartSend(" ");
  }

  fillBuf(0x0000);
  placeNavigation(OK);
  animText(0, 0, "Backup all words!", 0xffff, 1);
  animText(0, 10, "You will later use", 0xffff, 1);
  animText(0, 20, "them as login.", 0xffff, 1);
  wait(100);
  animText(0, 40, "Press OK to continue", 0xffff, 1); 
  waitForOK();
  // Paper backup here 
  wordposition = 0;
  update = true;
  setupDone = false;

  while (!setupDone) {
    switch(scanAnyKey()) {
      case 1:
        wordposition--;
        update = true;
        debounce();
        break;
      case 2:
        if(wordposition == Passphrase.indices_v)
          setupDone = true;
        debounce();
        break;
      case 3:
        wordposition++;
        update = true;
        debounce();
        break;
      default:
        break;
    }
    if(wordposition < 0)
      wordposition = Passphrase.indices_v;
    if(wordposition > Passphrase.indices_v)
      wordposition = 0;
    if(update) {
      if(wordposition >= 0 && wordposition < Passphrase.indices_v) {
        char number[9];
        itoaa((uint32_t)wordposition+1, number);
        fillBuf(0x0000);
        placeNavigation(LEFTRIGHT);
        //number of word
        animText(10, 10, "word #", 0xffff, 2);
        animText(80, 10, number, 0xffff, 2);
        animText(5, 70, mnemonicWord(Passphrase.indices[wordposition]), 0xffff, 2);
        update = false;
      }
      else if(wordposition == Passphrase.indices_v) {
        fillBuf(0x0000);
        placeNavigation(LEFTRIGHT);
        //number of word
        animText(10, 10, "Next?", 0xffff, 2);
        update = false;
      }
    }
  }
  
  // NOW CHECK EVERYTHING 
  //setupCheck(&Seed, &Passphrase); 

  //HASH THE ENTROOPY TO GET ENCRYPTION KEY 
  for(uint32_t i = 0; i<KECCAK_ROUNDS; i++) {
    keccak_256(Passphrase.entropy, KEYSIZE, Passphrase.entropy);
  }
  //encrypt
  uint8_t encrypted_seed[KEYSIZE] = {0};
  for(uint8_t i = 0; i < KEYSIZE; i++) {
    encrypted_seed[i] = Passphrase.entropy[i] ^ Seed.entropy[i];
  } 

  uartSend("Encrypted seed: ");
  uartSend(tohexLE(encrypted_seed, KEYSIZE)); 
  uartSend("\r\n");

  // and burn it into OTP
  //writeSeed(slot_num, encrypted_seed);
  //burnSetupByte(slot_num, 0x01);
  
  //setup secret for encrypted storage init
  memcpy(aes_key_array, Seed.entropy, KEYSIZE);
  //For decrypting memory, derive key for storage - from privkey
  deriveAesIv(aes_key_array, aes_init_vector);

  initPersistentStorage();


  SecretInitToZeroes(&Seed);
  SecretInitToZeroes(&Passphrase);

  fillBuf(0x0000);
  animText(0, 30, "DONE", 0xffff, 1); 
  waitForOK();
}

void setupCheck(SECRET *Seed, SECRET *Passphrase) {

  {
  fillBuf(0x0000);
  placeNavigation(OK);
  animText(0, 0, "Now re-enter your seed,", 0xffff, 1);
  animText(0, 10, "divide words by spaces.", 0xffff, 1);
  wait(100);
  animText(0, 30, "Press OK to continue", 0xffff, 1); 
  }
  waitForOK();

  bool check_ok = false;
  char input[256];// = "error website gables volcano opened pylons trying tinted soccer looking hickory lunar tuxedo roster wives bumper veered butter ozone dunes yoyo huts tsunami vials";
  uint8_t output[32] = {0};

  while(!check_ok) {
    memzero(output, KEYSIZE);
    memzero((uint8_t*)input, 256);
    keyboard(input, 256, "Seed:");
    wordsToBytes(input, strlen(input), output, KEYSIZE);
    fillBuf(0x0000);
    if (!memcmp(output, Seed->entropy, KEYSIZE)) {
      check_ok = true;
      animText(0, 10, "Seed is correct!", 0xffff, 1);
      wait(300);
    }
    else {
      fillBuf(0x0000);
      placeNavigation(OK);
      animText(0, 10, "Words don't match!", 0xffff, 1);
    }
  }


  uartSend("\r\n\r\nData decoded from user input: ");
  uartSend("\r\n");
  uartSend("Seed entropy: ");
  uartSend(tohexLE(output, KEYSIZE)); 
  uartSend("\r\n");


  fillBuf(0x0000);
  placeNavigation(OK);
  animText(0, 0, "Now passphrase,", 0xffff, 1);
  animText(0, 10, "divide words by spaces.", 0xffff, 1);
  wait(100);
  animText(0, 30, "Press OK to continue", 0xffff, 1); 
  waitForOK();

  check_ok = false;

  while(!check_ok) {
    memzero(output, KEYSIZE);
    memzero((uint8_t*)input, 256);
    keyboard(input, 256, "Passphrase:");
    wordsToBytes(input, strlen(input), output, KEYSIZE);
    fillBuf(0x0000);
    if (!memcmp(output, Passphrase->entropy, KEYSIZE)) {
      check_ok = true;
      animText(0, 10, "Passphrase correct!", 0xffff, 1);
      wait(300);
    }
    else {
      animText(0, 10, "Words don't match!", 0xffff, 1);
    }
  }

  uartSend("\r\n");
  uartSend("Passphrase entropy: ");
  uartSend(tohexLE(output, KEYSIZE)); 
  uartSend("\r\n");

  memzero(output, KEYSIZE);
  memzero((uint8_t*)input, 256);
}
*/

