/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <i_a@rmxwallet.io>
 * Copyright (C) 2019 m2049r@monerujo.io
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BUTTONS_H
#define BUTTONS_H
 
#include <stdbool.h>
#include "stdint.h"
#include "MCHP_MEC170x.h"
#include "display.h"
#include "util.h"

/*
//Button matrix:  FIXME to actual wiring!!!
|-----------------------------------------|
|         |  column1 | column2 | column3  |
|---------|----------|---------|----------|
|row4:    |    [7]   |   [8]   |   [9]    |
|row3:    |    [4]   |   [5]   |   [6]    |
|row2:    |    [1]   |   [2]   |   [3]    |
|row1:    |    [*]   |   [0]   |   [#]    |
|-----------------------------------------|

//Wired pins:
|------------------------------------------|
|         |  GPIO026 | GPIO031 | GPIO040   |
|---------|----------|---------|-----------|
|GPIO054  |    [7]   |   [8]   |   [9]     |
|GPIO053  |    [4]   |   [5]   |   [6]     |
|GPIO027  |    [1]   |   [2]   |   [3]     |
|GPIO120  |  <left>  |   <ok>  | <right>   |
|------------------------------------------|
rows -> outputs   (drive to 0)
columns -> inputs (pullups)
*/



/** Buttons pin interface setup **/
void buttonsInit(void);

/** All row pins to ZERO **/
void allRowsToZero(void);

/** All row pins to ONE **/
void allRowsToOne(void);

/** Set ROW(x) pin := 0 to enable scanning of that row if select==true  **/
void rowSelect(uint8_t row, bool select);

/** Reading col(x) pin **/
bool readCol(uint8_t col);

/** Basic algorithm for scanning the button matrix
   TODO prone to timing analysis FIXME **/
uint16_t scanMatrix(void);

/* Returns a number of pressed button, or -1 if nothing is pressed */
int8_t scanAnyKey(void);


/* 
  Brief: This function reflects the alphanumeric keyboard used in mobile phones.
  When typing, number 10 is deleting the last letter, number 12 is confirming the input ("OK")

  Inputs:
   *message:           String used for storing the input message
    size:              Size of the message string
    navigation_string: This string will appear at the top of the screen, when attempted to input a message

  Returns:
  The length of the input message
*/
uint16_t keyboard(char* message, uint16_t size, char navigation_string[]);

/** waiting for user to press OK **/
void waitForOK(void);

/** debounce button fnction **/
void debounce(void);


#endif
