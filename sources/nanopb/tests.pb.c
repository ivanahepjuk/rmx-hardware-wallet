/* Automatically generated nanopb constant definitions */
/* Generated by nanopb-0.4.0-dev */

#include "tests.pb.h"

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 40
#error Regenerate this file with the current version of nanopb generator.
#endif

PB_BIND(OutInitTesting, OutInitTesting, AUTO)


PB_BIND(InTestKeyImage, InTestKeyImage, AUTO)


PB_BIND(OutTestKeyImage, OutTestKeyImage, AUTO)



/* @@protoc_insertion_point(eof) */
