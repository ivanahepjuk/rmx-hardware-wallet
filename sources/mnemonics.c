/* This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <i_a@rmxwallet.io>, m2049r <m2049r@monerujo.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */
#include <string.h>
#include "mnemonics.h"
#include "english.h"
#include "uart.h"
#include "buttons.h"

// TODO: support other seed languages - beware of utf8 coding for crc
#define wordlist_electrum           wordlist_electrum_english



int bytesToWords(const uint8_t bytes[], char words[]) {

  uint16_t word_list_length = sizeof(wordlist_electrum) / sizeof(wordlist_electrum[0]);

  if (word_list_length != 1626) {
    return 1; // we can only do base 1626
  }

  uint32_t indices[25] = {0};

  // 4 bytes -> 3 words.  8 digits base 16 -> 3 digits base 1626
  for (uint8_t i = 0; i < 8; i++) {
    uint32_t val;        // each iteration are 4 chars from hexadecimal string
    memcpy((uint8_t*)&val, (uint8_t*)&bytes[i * 4], 4);

    uint32_t w[3];//, w2, w3; //three words
    w[0] = val % word_list_length;
    w[1] = ((val / word_list_length) + w[0]) % word_list_length;
    w[2] = (((val / word_list_length) / word_list_length) + w[1]) % word_list_length;

    //indices
    indices[3 * i] = w[0];
    indices[3 * i + 1] = w[1];
    indices[3 * i + 2] = w[2];

    for(uint8_t m = 0; m<3; m++) {
      if((strlen(wordlist_electrum[w[m]])) > 9){
        return 1;
      }
    // Adding the word
    strcat(words, wordlist_electrum[w[m]]);
    strcat(words, " ");
    }
  }

  // Constructing a string containing firts three letters from each word, then calculating checksum word based on this shortWords string:
  char shortWords[72];
  for(uint8_t n = 0; n<24; n++) {
    strncpy(shortWords + 3*n, wordlist_electrum[indices[n]], 3);
  }
  uint32_t crc32  = checksum_crc32(shortWords, 72, 0xFFFFFFFF);
  crc32 = (crc32 ^ 0xFFFFFFFF);
  indices[24] = indices[crc32 % 24];

  if((strlen(wordlist_electrum[indices[24]])) > 10){
        return 1;
  }

  // Adding the 25th checksum word to mnemonic words 
  strcat(words, wordlist_electrum[indices[24]]);

  return 0;
}


int wordsToBytes(const char words[], uint8_t bytes[]){
  // checking the number of words
  if (!words) {
    return 1;
  }

  uint32_t i = 0, j = 0;

  while (words[i]) {
    if (words[i] == ' ') {
      j++;
    }
    i++;
  }
  j++;

  if (j != 25) {
    return 1;
  }

  i = 0;
  j = 0;

  char word[4];                // Buffer, each found  word stored here
  uint32_t indices[24] = {0};  // Indice is a word's place in a dictionary

  // skip over ' '
  while (words[i]) {
    if(words[i] != ' ') { // Some word found
      word[0] = words[i];
      word[1] = words[i+1];
      word[2] = words[i+2];
      word[3] = '\0';     
      i = i+3;

      indices[j] = decodeWord(word); // Try to decode the word
      if (indices[j] == 0xffff)
        return 1;
      else 
        j++;
      //uartSend(word);
      //uartSend("\r\n");

      // Skip to next word
      while((words[i] != ' ') && (words[i])) {
        i++;
      }
    }
    i++;
  }
      
  // Computing triplets from indices
  for (uint8_t x=0; x<8; x++) {
    uint32_t compute_triplet[4] = {0};
    compute_triplet[1] = indices[x*3];
    compute_triplet[2] = indices[x*3 + 1];
    compute_triplet[3] = indices[x*3 + 2];
    compute_triplet[0]= compute_triplet[1] + 1626 * (((1626 - compute_triplet[1]) \
                      + compute_triplet[2]) % 1626) + 1626 * 1626 * (((1626 - compute_triplet[2]) \
                      + compute_triplet[3]) % 1626);

    if ((compute_triplet[0]% 1626 != compute_triplet[1])) {
      return 1;
    }

    memcpy((uint8_t*)&bytes[x*4], (uint8_t*)&compute_triplet[0], 4);
  }
  return 0;
}


uint16_t decodeWord(char input[]) {
  for(uint16_t i = 0; i < 1626; ++i) {
    if(!strncmp(wordlist_electrum_english[i], input, 3)) {
      return i;
    }
  }  
  //word not found
  return 0xffff;
}


const char* mnemonicWord(uint32_t word_idx) {
  // TODO check word_idx constraints (0, 1626)
  return wordlist_electrum[word_idx];
}


/*
 * CRC32 checksum
 *
 * Copyright (c) 1998-2003 by Joergen Ibsen / Jibz
 * All Rights Reserved
 *
 * http://www.ibsensoftware.com/
 *
 * This software is provided 'as-is', without any express
 * or implied warranty.  In no event will the authors be
 * held liable for any damages arising from the use of
 * this software.
 *
 * Permission is granted to anyone to use this software
 * for any purpose, including commercial applications,
 * and to alter it and redistribute it freely, subject to
 * the following restrictions:
 *
 * 1. The origin of this software must not be
 *    misrepresented; you must not claim that you
 *    wrote the original software. If you use this
 *    software in a product, an acknowledgment in
 *    the product documentation would be appreciated
 *    but is not required.
 *
 * 2. Altered source versions must be plainly marked
 *    as such, and must not be misrepresented as
 *    being the original software.
 *
 * 3. This notice may not be removed or altered from
 *    any source distribution.
 */

/*
 * CRC32 algorithm taken from the zlib source, which is
 * Copyright (C) 1995-1998 Jean-loup Gailly and Mark Adler
 */
static const uint32_t crc32tab[16] = {
    0x00000000, 0x1db71064, 0x3b6e20c8, 0x26d930ac, 0x76dc4190, 0x6b6b51f4,
    0x4db26158, 0x5005713c, 0xedb88320, 0xf00f9344, 0xd6d6a3e8, 0xcb61b38c,
    0x9b64c2b0, 0x86d3d2d4, 0xa00ae278, 0xbdbdf21c};

/* crc is previous value for incremental computation, 0xffffffff initially */
uint32_t checksum_crc32(char *data, uint32_t length, uint32_t crc) {
  for (uint32_t i = 0; i < length; ++i) {
    crc ^= data[i];
    crc = crc32tab[crc & 0x0f] ^ (crc >> 4);
    crc = crc32tab[crc & 0x0f] ^ (crc >> 4);
  }

  // return value suitable for passing in next time, for final value invert it
  return crc; /* ^ 0xFFFFFFFF */
}



