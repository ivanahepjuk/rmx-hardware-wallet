/*
* This file is a part of RMX HARDWARE WALLET
*
* This program is used exclusively under the belowmentioned license provided by 
* Microchip Technology Inc. and its subsidiaries and with the full compliance 
* with it. It is your responsibility to comply with these license terms:
*****************************************************************************
* © 2018 Microchip Technology Inc. and its subsidiaries. 
* 
* Subject to your compliance with these terms, you may use Microchip software
* and any derivatives exclusively with Microchip products. It is your 
* responsibility to comply with third party license terms applicable to your 
* use of third party software (including open source software) that may 
* accompany Microchip software.
*   
* THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  
* NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, 
* INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, 
* AND FITNESS FOR A PARTICULAR PURPOSE. 
* 
* IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
* INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
* WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS 
* BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  
* TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
* CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
* OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
*****************************************************************************/

#include "systick.h"
#include "core_cm4.h"
#include "led.h"
#include "system_MEC170x.h"

static uint32_t sm_ticks = 0;
uint32_t clk_mhz;


void SysTick_Init(void) {
  clk_mhz = SystemCoreClock / 1000000;
  SysTick_Config(SystemCoreClock / 1000); // 1ms
}


void SysTick_Handler(void) {
  sm_ticks++;
  if (!(sm_ticks % 500)) {;};//ledToggle(LED_RED);
}


void HardFault_Handler(void) {
  ledBlink(SHORT);
}


uint32_t sm_now_ms(void) {
  return sm_ticks;
}


void sm_delay_us(uint32_t t) {
  uint32_t cnt,cnt1;
  uint32_t diff;

  t *= clk_mhz;
   
  cnt = SysTick->VAL;
  while (t) {
    cnt1 = SysTick->VAL;
    if (cnt >= cnt1) {
      diff = (cnt - cnt1);
    } else {
      diff = cnt + (SysTick->LOAD - cnt1);
    }
            
    if (t > diff){
      t = t - diff;
    } else {
      t = 0;
    }
    cnt = cnt1;     
  }  
}

