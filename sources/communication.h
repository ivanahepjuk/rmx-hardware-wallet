/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

/*
(1) Structure of each protobuf communication payload, going in and out:

   Each exchanged message consist of five parts:

   +-------------+------------+------------+-----------------+------------------+-------------+
   |             | START_BYTE | IDENTIFIER | size_of_message | protobuf message | 8B checksum |
   +-------------+------------+------------+-----------------+------------------+-------------+
   | byte offset |     0      |     1      |      2-3        |       4-N        |   N-(N+8)   |
   +-------------+------------+------------+-----------------+------------------+-------------+
   | example     |    0x81    |    0x82    |   0x00 0x80     |    Bytearray     |    0xAA     |
   +-------------+------------+------------+-----------------+------------------+-------------+

   Both sides listen in loop, transmission starts after receiving START_BYTE. 
   From IDENTIFIER they know how to decode message. 
   From size_of_message + 8 they know payload's length including checksum.

(2) Naming convention for message structures

   Both - both sides could use this message
   Out - this message is going from RMX to node
   In - this messge is going from node to RMX


*/
#ifndef COMMUNICATION_H
#define COMMUNICATION_H

#include "stdint.h"
#include "uart.h"
#include "display.h"
#include "led.h"
#include "buttons.h"
#include "util.h"
#include "ui-functions.h"

//#include <pb_encode.h>
#include <pb_decode.h>
#include "monero.pb.h"

/** Others **/
#define TIMEOUT                       1000000
#define START_TRANSMISSION_BYTE          0x81

/** system-messages identifiers **/
#define OUT_DEBUG                        0xff

/** monero messages identifiers **/
#define OUT_INIT_SCANNING                0x84
#define IN_INIT_SCANNING                 0x85
#define OUT_GET_TRANSACTION_BATCH        0x86
#define IN_GET_TRANSACTION_BATCH         0x87
#define OUT_GET_TRANSACTION_DETAILS      0x88
#define IN_GET_TRANSACTION_DETAILS       0x89
#define OUT_IS_SPENT                     0x92
#define IN_IS_SPENT                      0x93
#define OUT_MONERO_ADDRESS               0x94

#define IN_STATUS_MESSAGE                0x95

#define BATCH_SIZE                      16384 
#define TX_DATA_SIZE                    16384

/** xmpp messages identifiers **/
#define XMPP_LOGIN_OUT                   0x40
#define XMPP_LOGIN_IN                    0x41
#define XMPP_ADD_CONTACT_OUT             0x42
#define XMPP_ADD_CONTACT_IN              0x43
#define XMPP_QUERY_UNREAD_OUT            0x44
#define XMPP_QUERY_UNREAD_IN             0x45

#define XMPP_QUERY_MESSAGE_OUT           0x46 // Send me oldest message for given ID
#define XMPP_QUERY_MESSAGE_IN            0x47 // Here you are the oldest message for given ID
#define XMPP_MESSAGE_ACK_OUT             0x48 // I readed one message, please delete it from server
#define XMPP_MESSAGE_ACK_IN              0x49 // Ok I deleted
#define XMPP_MESSAGE_OUT                 0x50 // Sending out a structure with encrypted essage, metadata and jid
#define XMPP_MESSAGE_IN                  0x51 // Response
#define XMPP_MESSAGE_DELETE_OUT          0x52 // Delete last message from server?

/** Testing messages **/
#define TEST_OUT_INIT_TESTING              10
#define TEST_IN_INIT_TESTING               11
#define TEST_IN_TEST_KEY_IMAGE             12
#define TEST_OUT_TEST_KEY_IMAGE            13


/** Buffers are used also externally with different functions for nanopb communication **/
#define BUFFER_OUT_SIZE      (256 + 256)
#define BUFFER_IN_SIZE       (BATCH_SIZE + 128)
extern uint8_t nanopb_buffer_out[BUFFER_OUT_SIZE]; //FIXME maybe there are better sizes :O) ? will see later
extern uint8_t nanopb_buffer_in[BUFFER_IN_SIZE]; 


/* Send payload over serial port */
bool sendMessage(const uint8_t id, const pb_msgdesc_t *fields, void *msg);


/* Receiving payload over serial port */
bool receiveMessage(const uint8_t id,  const pb_msgdesc_t *fields, void *msg, const uint32_t timeout);


/* Sending debug messages to pyrmx, so they can be displayed in pyrmx debug log */
bool debug(char message[]);

#endif
