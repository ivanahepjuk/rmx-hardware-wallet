#!/usr/bin/env python2
import time
import random
import sys

from pylibftdi import Driver, Device
from hashlib import sha256
from binascii import hexlify
from pylibftdi import FtdiError
import sys

print "spi_flashing.py runs python version"
print(sys.version)



# smycka, ktera cekuje jestli je pripojene zarizeni
def connect_device():

    not_connected = 1

    print "--> Waiting for commands.."

    while not_connected:
        
        if (dev.read(1) == ('a')):
            dev.write('A')
            print "--> First byte exchanged \tOK"

            while 1:
                if (dev.read(1) == ('b')):
                    dev.write('B')
                    print "--> Second byte exchanged \tOK"
                    not_connected = 0
                    break
            time.sleep(0.2)
        time.sleep(0.2)

    print "--> DEVICE CONNECTED"


# vytvori v PC 32B nahod, posle je do RMX, to je vezme a vynasobi ED25519 a vysledek posle zpet
def send_random_to_multiply():

    komunikace = 1

    while komunikace:
        dev.flush()  #radsi to tu dam
        #print "--> Waiting for commands"
        if (dev.read(1) == ('c')):  #n.1 hash button pressed
            SENDING = bytearray(random.getrandbits(8) for _ in xrange(32))
            print "--> ED25519 point multiplication:"
            dev.write('C')  #handshake
            dev.write(SENDING)
            print "--> Random integer sent to RMX: \t\"" + hexlify(SENDING) + "\""
            #hash_object = sha256(SENDING)
            #hex_dig_sending = hash_object.hexdigest()
            #print "--> sha256 hash of random string: \"" + hex_dig_sending + "\"" #
            while 1:
                if (dev.read(1) == ('d')):
                    dev.write('D')
                    #for i in range(32):
                    RECEIVING = dev.read(32)
                    print "--> ed25519 point multiply result: \t\"" + hexlify(RECEIVING) + "\""
                    komunikace = 0
                    break
            #time.sleep(0.2)
        #time.sleep(0.2)


########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################

# START!!
#



# Debug:
# dev_list = Driver().list_devices()
# if len(dev_list) != 0:
#     print "Following devices found:"
# for device_ in dev_list:
#     print device_

# pylibftdi is searching serial number '524D58', then conects interface B to it
try:
    dev = Device(device_id="524D58", mode='b', interface_select=2)
except FtdiError as err:
    msg = ("RMX not connected. Please connect RMX device and then run this "
           "script again")
    print msg
    # print err # uncoment to see the actual err
    sys.exit(1)

print "--> RMX device found!"


# Baudrate je rychlost, zhruba to odpovida bitum/s
dev.baudrate = 115200
dev.flush()

connect_device()
while 1:
    send_random_to_multiply()
#to dev.close se asi nezavola nikdy protoze while 1 o par radku vyse
dev.close()
