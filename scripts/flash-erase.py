#!/usr/bin/env python3

# Based on Python FTDI SPI example from iosoft.blog
# Compatible with Python 2.7 or 3.x

#
# This file is a part of RMX HARDWARE WALLET
#
# Copyright (C) 2018 i_a <i_a@rmxwallet.org>
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library.  If not, see <http://www.gnu.org/licenses/>.
#

FTD2XX = False  # Set False if using pylibftdi
FTDI_TIMEOUT = 1000  # Timeout for D2XX read/write (msec)

if FTD2XX:
    import sys, time, ftd2xx as ftd
else:
    import sys, time, pylibftdi as ftdi

from binascii import hexlify
from hashlib import sha256
import math
import os
import sys
import time

print("spi_flashing.py runs python version")
print(sys.version)

#command data   mask  comment
#0x80      B     B    write low byte
#0x82      B     B    write high byte
#0x81                 read low byte
#0x83                 read high byte

OPS =   0x03    # Bit mask for SPI clock and data out  0b11
CS =    0x40     # Bitmask for CHIP SELECT
RESET = 0x5 # Bitmask for reset
LED =   0x1 # Bitmask for reset



# Set mode (bitbang / MPSSE)
def set_bitmode(d, bits, mode):
    return (d.setBitMode(bits, mode) if FTD2XX else d.ftdi_fn.ftdi_set_bitmode(
        bits, mode))


# Open device for read/write
def ft_open(n=0):
    if FTD2XX:
        d = ftd.open(n)
        d.setTimeouts(FTDI_TIMEOUT, FTDI_TIMEOUT)
    else:
        d = ftdi.Device(
            device_id="524D58",
            mode='b',
            interface_select=1,
            auto_detach='true')
    return d


# Set SPI clock rate
def set_spi_clock(d, hz):
    div = int((12000000 / (hz * 2)) - 1)  # Set SPI clock
    ft_write(d, (0x86, div % 256, div // 256))


# Read byte data into list of integers
def ft_read(d, nbytes):
    s = d.read(nbytes)
    return [ord(c) for c in s] if type(s) is str else list(s)


# Write list of integers as byte data
def ft_write(d, data):
    s = str(bytearray(data)) if sys.version_info < (3, ) else bytes(data)
    return d.write(s)


# Write MPSSE command with word-value argument
def ft_write_to_flash(d, cmd, data):
    n = len(data) - 1
    ft_write(d, [cmd, n % 256, n // 256] + list(data))

import time, sys
import re


class ProgressBar(object):
    # courtesy of https://stackoverflow.com/a/36985003/4050925
    DEFAULT = 'Progress: %(bar)s %(percent)3d%%'
    FULL = '%(bar)s %(current)d/%(total)d (%(percent)3d%%) %(remaining)d to go'

    def __init__(self, total, width=40, fmt=DEFAULT, symbol='=',
                 output=sys.stderr):
        assert len(symbol) == 1

        self.total = total
        self.width = width
        self.symbol = symbol
        self.output = output
        self.fmt = re.sub(r'(?P<name>%\(.+?\))d',
            r'\g<name>%dd' % len(str(total)), fmt)

        self.current = 0

    def __call__(self):
        percent = self.current / float(self.total)
        size = int(self.width * percent)
        remaining = self.total - self.current
        bar = '[' + self.symbol * size + ' ' * (self.width - size) + ']'

        args = {
            'total': self.total,
            'bar': bar,
            'current': self.current,
            'percent': percent * 100,
            'remaining': remaining
        }
        print('\r' + self.fmt % args, file=self.output, end='')

    def done(self):
        self.current = self.total
        self()
        print('', file=self.output)

if __name__ == "__main__":
    dev = ft_open(0)
    dev.flush()
    dev.close()
    dev = ft_open(0)
   

    #if dev:
    print("erasing SST26VF016B(A) memory...")
    #print("(TODO: It would be nice to check exact memory type by reading JEDEC header)")
    #print("Also there is possible bug in future, when image to flash is bigger than 256*256B")
    set_bitmode(dev, OPS, 2)  # Set SPI mode
    set_spi_clock(dev, 6000000)  # Set SPI clock

    #write enable 0x06
    ft_write(dev, (0x80, 0, OPS + CS))  # Set outputs + CS pin
    ft_write_to_flash(dev, 0x11, [0x06])
    ft_write(dev, (0x80, 0, OPS))  # Set outputs + CS pin
    time.sleep(0.01)

    #unlock the flash 0x98
    ft_write(dev, (0x80, 0, OPS + CS))  # Set outputs + CS pin
    ft_write_to_flash(dev, 0x11, [0xC7])
    ft_write(dev, (0x80, 0, OPS))  # Set outputs + CS pin
    time.sleep(0.1)

 

    print("Erased.")


    print("MCU into reset...")
    ft_write(dev, (0x82, 0, RESET))  # Set outputs + CS pin
    ft_write(dev, (0x82, 1, LED))  # Set outputs + CS pin
    time.sleep(0.1)
    #reset up
    ft_write(dev, (0x82, 1, RESET))  # Set outputs + CS pin
    ft_write(dev, (0x82, 0, LED))  # Set outputs + CS pin
    print("MCU out of reset...")


    dev.flush()
    dev.close()

# EOF
