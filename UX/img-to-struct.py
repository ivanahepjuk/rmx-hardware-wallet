# Imported PIL Library from PIL import Image
# gimp output picture in indexed mode!!
from PIL import Image
filename = "setup_logo_64x64"
im = Image.open(filename + '.bmp')
width, height = im.size
px = im.load()

with open("output.c", "w") as fo:
    fo.write("const uint8_t " + filename + "_data [] = {")
    #print ('bytearray size:')
    #print ((width/8)*height)

    byte = 0

    for i in range(height):
        for j in range(width):
            #    print (px[j,i])
            if (px[j, i]) > 0:
                byte |= 0x01

            if (((j + 1) % 8) == 0):
                fo.write("\\x{:02X}".format(byte))
                byte = 0
                fo.write(",")

            byte = byte << 1
        fo.write("\n")
    fo.write("};")

#######

#{60,36,34,66,254,0}
