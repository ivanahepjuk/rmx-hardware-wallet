/* This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */


#include "main.h"
#include "CEC1702_defines.h"

#include "i2c.h"
#include "atecc508a.h"
#include "efuse.h"


#include "mnemonics.h"
#include "xmpp.h"
#include "crypto.h"
#include "flash_memory.h"
#include "storage.h"

#include "rng.h"
#include "login.h"
#include "tests.h"




int main(void) {

  DEBUG_ENABLE_REGISTER |= 0b1101;//1;

  initCEC();

  //char bug[] = "aaaabbbbccccddddAAAABBBBCCCCDDDDaaaabbbbccccddddAAAABBBBCCCCDDDDaaaabbbbccccddddAAAABBBBCCCCDDDDaaaabbbbccccddddAAAABBBBCCCCDDDD";

  //uxLogoWelcome();

  login();
 
  ledBlink(SHORT);

  int start_position = 1;
  int position = start_position;
  bool move = true;
  while (1) {
    switch(scanAnyKey()) {
      case 1:
        position--; 
        //debounce();
        move = true;
        break;
      case 2:
        if(position == 1) {
          debounce();
          //login();
          //moneroSynchronize();
          showMenu(&Monero);
        } 
        if(position == 2) {
          debounce();
          login();
          //  wait(700);
          showMenu(&Xmpp);          
        }
        if(position == 3) {
          debounce();
          showMenu(&Rng);
        }
        if(position == 4) {
          login();
          debounce();
          showMenu(&Setup);
        }
        if(position == 5) {
          debounce();
          showMenu(&Experimental);
        }
        move = true;
        break;
      case 3:
        position++; 
        //debounce();
        move = true;
        break;
      default:
        break;
    }

    if (position > 5)
      position = 0;
    if (position < 0)
      position = 5;
    if(move) {
      /* animate pictures on display */
      switch(position) {
        case 0:
          break;
        case 1:
          placePictureWithAnimtext(32,15,&monero_64x64, 95, "MONERO", RED, 1);
          break;
        case 2:
          placePictureWithAnimtext(32,15,&messenger_64x64, 95, "MESSENGER", RED, 1);
          break;
        case 3:
          placePictureWithAnimtext(32,15,&rng_64x64, 95, "RANDOM NUMBERS", RED, 1);
          break;
        case 4:
          placePictureWithAnimtext(32,15,&setup_64x64, 95, "SETUP", RED, 1);
          break;
        case 5:
          placePictureWithAnimtext(32,15,&experimental_64x64, 95, "EXPERIMENTAL", RED, 1);
          break;
        default:
          break;
      }
    }
  move = false;
  }
  return 0;
}



void __attribute__ ((section(".after_vectors"),noreturn)) _start(void) {
  SystemInit();
  main();
  for (;;);
}



/*

    while(1) {
      while(!(UART_LINE_STATUS_REGISTER & 0x01))  //DATA READY
        asm("mov r0,r0");//DO NOTHING
      uint32_t received = UART_RECEIVE_BUFFER_REGISTER;
      char number[9];
      itoaa((uint32_t)received, number);
      fillBuf(0x0000);
      //number of word
      animText(10, 10, "Received #", 0xffff, 2);
      animText(80, 10, number, 0xffff, 2);
    }
*/

/*
cs     3v3
sio1   sio3
sio2   clk
gnd    sio0




*/



